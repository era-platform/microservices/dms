#!/usr/bin/env escript
%% -*- erlang -*-
%%! -smp enable -sname add_port_env
main(Args) when is_list(Args) ->
	Res = lists:map(fun(Arg) -> add_port_env(Arg) end, Args),
	case lists:all(fun(ok) -> true; (_) -> false end, Res) of
		true -> ok;
		false -> halt(1)
	end.

-define(OUT(Fmt,Args), io:format(Fmt++"~n", Args)).

-define(ERL_LDFLAGS, {"linux","ERL_LDFLAGS","-L$ERL_EI_LIBDIR -lei"}).

add_port_env(Filename) ->
	case file:consult(Filename) of
		{ok, Terms} ->
            ?OUT("Terms = ~120tp", [Terms]),
            ROA = replace_or_add_ldflags(Terms),
            ?OUT("ROA = ~120tp", [ROA]),
            case ROA of
            %case replace_or_add_ldflags(Terms) of
                ok -> ok;
                {ok, NewTerms} ->
                    ?OUT("NewTerms = ~120tp", [NewTerms]),
                    WS = write_terms(Filename, NewTerms),
                    ?OUT("write_terms(~120tp, NewTerms) => ~120tp", [Filename, WS]),
                    WS;
                {error,_}=Err -> Err % not a case
            end;
		{error,_}=Err2 ->
            ?OUT("file:consult ~120tp", [Err2]),
            Err2
	end.

replace_or_add_ldflags(Terms) when is_list(Terms) ->
    % cases:
    % {port_env} does not exist -> add new, return changed;
    % {port_env} exists, ldflags does not exist -> add new, return changed;
    % {port_env} exists, ldflags exists, matches -> return ok;
    % {port_env} exists, ldflags exists, does not match -> return error.
    F = fun({port_env,PortEnv}=T, {Acc,_}) ->
                case modify_portenv(PortEnv) of
                    ok -> {[T | Acc], matched};
                    {ok, NewPortEnv} -> {[NewPortEnv | Acc], changed}
                end;
           (T, {Acc,X}) -> {[T | Acc], X}
        end,
    T2 = lists:foldr(F, {[],unchanged}, Terms),
    ?OUT("T2 = ~120tp", [T2]),
    case T2 of
    %case lists:foldr(F, {[],unchanged}, Terms) of
        {_,matched} -> ok;
        {_,unchanged} -> {ok, [{port_env,[?ERL_LDFLAGS]}]};
        {NewTerms,changed} -> {ok,NewTerms}
    end.

modify_portenv(PortEnv) when is_list(PortEnv) ->
    F = fun(?ERL_LDFLAGS, {Acc,_}) -> {[?ERL_LDFLAGS | Acc], matched};
           ({"linux","ERL_LDFLAGS",_}, {Acc,_}) -> {[?ERL_LDFLAGS | Acc], unmatched};
           (T, {Acc,X}) -> {[T | Acc], X}
        end,
    case lists:foldr(F, {[],nx}, PortEnv) of
        {_,matched} -> ok;
        {PE,unmatched} -> {ok,PE}; % XXX just return replaced ERL_LDFLAGS
        {_,nx} -> {ok, [?ERL_LDFLAGS | PortEnv]}
    end.

write_terms(Filename, Terms) ->
	FormatFun = fun(Term) -> io_lib:format("~tp.~n", [Term]) end,
	Text = unicode:characters_to_binary(lists:map(FormatFun, Terms)),
	file:write_file(Filename, Text).
