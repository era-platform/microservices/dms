%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.05.2021
%%% @doc Validates 'classes' entity on any modify operation.

-module(dms_validator_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4,
         decorate_fixture/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=EntityInfo,ModifierInfo) ->
    Fixtures = [?FixtureClasses:storages(Domain),?FixtureClasses:classes(Domain)],
    {Ids,CNs} = lists:unzip(lists:map(fun(Item) -> {maps:get(<<"id">>,Item),maps:get(<<"classname">>,Item)} end, Fixtures)),
    FunIsFixt = fun(E) -> E /= undefined andalso (lists:member(maps:get(<<"id">>,E),Ids) orelse lists:member(maps:get(<<"classname">>,E),CNs)) end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        true when Operation=='delete', ModifierInfo /= {'fixturer',?ClassesCN} ->
            {error,{invalid_operation,<<"Access denied. Cannot remove system classes directly. Modify configuration instead.">>}};
        _ when Operation=='delete' ->
              {ok,undefined};
        true when Operation=='create'; Operation=='replace'; Operation=='update' ->
            check_fixtures(Domain,Operation,EntityInfo,ModifierInfo,Fixtures);
        _ ->
            case check_entity(Domain,Operation,E1) of
                {error,_}=Err -> Err;
                {ok,EntityV} ->
                    case validate_all_classes(Domain,Operation,{E0,EntityV}) of
                        ok -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% --------------------------------------
%% Helps to format and decorate fixture
%% --------------------------------------
-spec decorate_fixture(Domain::binary() | atom(), Entity::map()) -> {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
decorate_fixture(Domain,Entity) ->
    case check_entity(Domain,'fixture',Entity) of
        {error,_}=Err -> Err;
        {ok,EntityV} -> {ok,EntityV}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check if intersects fixtures
%% -------------------------------------
check_fixtures(_Domain,_Operation,{_Id,_E0,E1},ModifierInfo,Fixtures) ->
    Keys = [<<"id">>,<<"classname">>,<<"storage_mode">>,<<"cache_mode">>,<<"integrity_mode">>,<<"properties">>,<<"opts">>],
    case lists:member(maps:with(Keys,E1),Fixtures) of
        false when ModifierInfo /= {fixturer,?ClassesCN} ->
            % TODO: check E1 to fixture, but no modifier_info
            {error,{invalid_operation,<<"Access denied. Cannot modify system classes directly. Modify configuration instead.">>}};
        _ -> {ok,E1}
    end.

%% -------------------------------------
%% Check class properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_1(Domain,Operation,Item).

%% @private
% classname
check_1(Domain,Operation,Item) ->
    case maps:get(<<"classname">>,Item) of
        <<>> -> {error, {invalid_params, <<"field=classname|Invalid 'classname'. Must not be empty">>}};
        ClassName when is_binary(ClassName) ->
            SF = binary:first(ClassName),
            SL = binary:last(ClassName),
            F = fun(A) when (A==$/)
                        orelse (A>=$a andalso A=<$z)
                        orelse (A>=$A andalso A=<$Z)
                        orelse (A>=$0 andalso A=<$9)
                        orelse (A==$_) orelse (A==$-) orelse (A==$~) -> true;
                   (_) -> false end,
            case lists:all(F, ?BU:to_list(ClassName)) of
                true when SF/=$/, SL/=$/ -> check_1b(Domain,Operation,Item);
                true -> {error, {invalid_params, <<"field=classname|Invalid classname. Should not start or end with '/'">>}};
                false -> {error, {invalid_params, <<"field=classname|Invalid classname. Contains invalid symbols. Expected: [A-Za-z0-9_-~/]">>}}
            end end.
% parent_id
check_1b(Domain,Operation,Item) ->
    EmptyId = ?BU:emptyid(),
    case maps:get(<<"parent_id">>,Item,<<>>) of
        <<>> -> check_2(Domain,Operation,Item#{<<"parent_id">> => EmptyId});
        EmptyId -> check_2(Domain,Operation,Item);
        IdClass ->
            case catch ?BU:to_guid(IdClass) of
                {'EXIT',_} -> {error, {invalid_params, <<"field=parent_id|Invalid parentPid. Expected uuid or empty string">>}};
                EmptyId -> {error, {invalid_params, <<"field=parent_id|Invalid parentPid. Expected uuid or empty string">>}};
                IdClass -> check_2(Domain,Operation,Item);
                IdClass1 -> check_2(Domain,Operation,Item#{<<"parent_id">> => IdClass1})
            end end.
% storage_mode
check_2(Domain,Operation,Item) ->
    case maps:get(<<"storage_mode">>,Item) of
        <<"runtime">> -> check_3(Domain,Operation,Item);
        <<"history">> -> check_3(Domain,Operation,Item);
        <<"category">> -> check_3(Domain,Operation,Item);
        <<"transactionlog">> -> check_3(Domain,Operation,Item);
        <<"ram">> -> check_3(Domain,Operation,Item);
        <<"abstract">> -> check_3(Domain,Operation,Item);
        _ -> {error, {invalid_params, <<"field=storage_mode|Invalid storage_mode. Expected 'runtime', 'history', 'category', 'transactionlog', 'ram', 'abstract'">>}}
    end.
% integrity_mode
check_3(Domain,Operation,Item) ->
    case maps:get(<<"integrity_mode">>,Item) of
        <<"async">> -> check_4(Domain,Operation,Item);
        <<"sync">> -> check_4(Domain,Operation,Item);
        <<"sync_fast_read">> -> check_4(Domain,Operation,Item);
        <<"sync_fast_notify">> -> check_4(Domain,Operation,Item);
        _ -> {error, {invalid_params, <<"field=integrity_mode|Invalid integrity_mode. Expected 'async', 'sync', 'sync_fast_read', 'sync_fast_notify'">>}}
    end.
% cache_mode
check_4(Domain,Operation,Item) ->
    StorageMode = maps:get(<<"storage_mode">>,Item),
    case maps:get(<<"cache_mode">>,Item) of
        <<"full">> when StorageMode==<<"history">>; StorageMode==<<"transactionlog">> ->
            {error, {invalid_params, ?BU:strbin("field=cache_mode|Invalid cache_mode. Expected 'temp', 'none' for storage_mode='~ts'",[StorageMode])}};
        <<"full">> -> check_5(Domain,Operation,Item);
        <<"temp">> -> check_5(Domain,Operation,Item);
        <<"none">> -> check_5(Domain,Operation,Item);
        _ -> {error, {invalid_params, <<"field=cache_mode|Invalid cache_mode. Expected 'full', 'temp', 'none'">>}}
    end.
% properties
check_5(Domain,Operation,Item) ->
    case check_properties(Domain,Operation,Item) of
        {ok,Item1} -> check_6(Domain,Operation,Item1);
        {error,_}=Err -> Err
    end.
% opts
check_6(Domain,Operation,Item) ->
    Opts = maps:get(<<"opts">>,Item),
    DefaultOpts = default_opts(),
    case check_opts(Domain,Operation,maps:merge(DefaultOpts,maps:with(maps:keys(DefaultOpts), Opts)),Item) of
        {error,_}=Err -> Err;
        Opts1 -> check_x(Domain,Operation,Item#{<<"opts">> => Opts1})
    end.
%
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% -------------------------------------
%% Check properties
%% -------------------------------------

check_properties(Domain,Operation,Item) ->
    Props = maps:get(<<"properties">>,Item),
    check_properties_1(Props,Domain,Operation,Item).
%% value type, required keys, enumerations,
check_properties_1(Props,Domain,Operation,Item) ->
    Res = lists:foldl(fun(_,{error,_}=Err) -> Err;
                         (Prop,_) when not is_map(Prop) -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties. List of properties should contain objects",[])}};
                         (Prop,Acc) ->
                            K = case maps:get(<<"name">>,Prop,undefined) of
                                    undefined -> {error,{invalid_params, <<"field=properties|Invalid properties. Property object should contain field 'name'">>}};
                                    <<"id">> -> {error,{invalid_params, <<"field=properties|Invalid properties. List should not contain property 'id'">>}};
                                    <<"item_json_data">> -> {error,{invalid_params, <<"field=properties|Invalid properties. List should not contain property 'item_json_data'">>}};
                                    Name -> Name
                                end,
                            case K of
                                {error,_}=Err -> Err;
                                _ ->
                                    case check_property(K,Prop,Domain,Operation,Item) of
                                        {error,_}=Err -> Err;
                                        {ok,V1} -> [V1|Acc]
                                    end
                            end
                      end, [], Props),
    case Res of
        {error,_}=Err -> Err;
        _ when is_list(Res) -> {ok,Item#{<<"properties">> => lists:reverse(Res)}}
    end.

%% caption
check_property(K,V,Domain,Operation,Item) ->
    case maps:get(<<"caption">>,V,undefined) of
        undefined -> check_property_1(K,V,Domain,Operation,Item);
        Caption when is_binary(Caption), size(Caption)<500 -> check_property_1(K,V,Domain,Operation,Item);
        _ -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) caption. Expected string less than 500 bytes",[K])}}
    end.
%% required
check_property_1(K,V,Domain,Operation,Item) ->
    case maps:get(<<"required">>,V,undefined) of
        B when B==true;B==false -> check_property_2(K,V,Domain,Operation,Item);
        _ -> check_property_2(K,maps:without([<<"required">>],V),Domain,Operation,Item)
    end.
%% multi
check_property_2(K,V,Domain,Operation,Item) ->
    case maps:get(<<"multi">>,V,undefined) of
        B when B==true;B==false -> check_property_3(K,V,B,Domain,Operation,Item);
        _ -> check_property_3(K,maps:without([<<"multi">>],V),false,Domain,Operation,Item)
    end.
%% data_type
check_property_3(K,V,Multi,Domain,Operation,Item) ->
    ATs = available_types(),
    case maps:get(<<"data_type">>,V,undefined) of
        undefined -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts). Value should contain 'data_type'",[K])}};
        DT ->
            case lists:member(DT,ATs) of
                false -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) data_type. Expected: ~ts",[K,?BU:join_binary_quoted(ATs,<<"'">>,<<", ">>)])}};
                true -> check_property(DT,Multi,K,V,Domain,Operation,Item)
            end end.

%% @private
available_types() ->
    [<<"string">>, <<"boolean">>,
     <<"integer">>, <<"long">>, <<"float">>,
     <<"date">>, <<"time">>, <<"datetime">>,
     <<"uuid">>,
     <<"enum">>,
     <<"any">>,
     <<"entity">>,
     <<"attachment">>].

%% @private
check_property(<<"string">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"boolean">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"integer">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"long">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"float">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"date">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"time">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"datetime">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"duration">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"uuid">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V};
check_property(<<"enum">>,_M,K,V,_Domain,_Operation,_Item) ->
    case maps:get(<<"items">>,V,undefined) of
        Items when is_list(Items) ->
            Items1 = lists:usort(Items),
            case lists:all(fun(I) -> is_binary(I) andalso I/=<<>> end, Items1) of
                true -> {ok,V#{<<"items">> => Items1}};
                false -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) itess. Expected list of non-empty string values",[K])}}
            end;
        _ -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) items. Expected list of non-empty string values",[K])}}
    end;
check_property(<<"any">>,Multi,K,V,_Domain,_Operation,_Item) ->
    case maps:get(<<"merge_levels">>,V,undefined) of
        undefined -> {ok,V};
        _ when Multi -> {ok,maps:without([<<"merge_levels">>],V)};
        I when is_integer(I), I>=0 -> {ok,V};
        _ -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) merge_levels. Expected non-negative integer value",[K])}}
    end;
check_property(<<"entity">>,_M,K,V,Domain,_Operation,Item) ->
    Id = maps:get(<<"id">>,Item),
    IsLoading = false,
    case maps:get(<<"idclass">>,V,undefined) of
        IdClass when is_binary(IdClass), IsLoading -> {ok,V};
        IdClass when is_binary(IdClass) ->
            case ?DMS_CACHE:read_cache(Domain,?ClassesCN,#{},auto) of
                {ok,Classes,_} ->
                    case lists:filter(fun(ClassItem) -> maps:get(<<"id">>,ClassItem)==IdClass end, Classes) of
                        [] when IdClass/=Id -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) idclass. Expected existing idclass",[K])}};
                        _ -> {ok,V}
                    end;
                T ->
                    ?LOG('$error',"Validator read classes: ~120tp",[T]),
                    {error,{internal_error,<<"Validator cannot read classes">>}}
            end;
        _ -> {error,{invalid_params, ?BU:strbin("field=properties|Invalid properties(~ts) idclass. Expected existing idclass",[K])}}
    end;
check_property(<<"attachment">>,_M,_K,V,_Domain,_Operation,_Item) -> {ok,V}.

%% -------------------------------------
%% Check opts
%% -------------------------------------

%% @private
default_opts() ->
    #{
        <<"comment">> => <<>>,
        <<"title">> => <<>>,
        % common
        <<"validator">> => <<>>, % global name of validator service that should be requested to validate changes before apply (save/notify). When entity is ready to store, process synchronously calls validator to check and modify entity.
        <<"check_required_fill_defaults">> => true,  % if entities should be checked for required fields and filled by default values by the server. Model can economy resources by disable this on high-load collections.
        <<"max_limit">> => 100, % how many items could be requested on read in max. Any read of collection is limited by this value. 10-10000.
        <<"max_mask">> => [], % max mask of properties, that could be returned on collection read.
        <<"replace_without_read">> => false, % setup implementation when replace operation doesn't use previously find/read. Notifications would be filtered only by new replacing entity.
        <<"notify">> => true, % if subscribers should be notified on modifications.
        <<"notify_integrity_timeout">> => 0, % if notify is failed on getting subscriptions from subscr (noproc, etc), then it could wait and retry for timeout, other notifications would wait in queue. External property.
        <<"appserver">> => <<>>, % global name of appserver service (call from ws and other clients instead of dms, it's responsibility of appserver to store to dms). External property.
        <<"store_changehistory_mode">> => <<"none">>, % store changes in modelevents. none | sync | async
        % <<"dms_group">> => #{}, % Site -> role group index. Default - unique (less number) dms group for domain.
        % for storage_mode = 'ram', 'runtime'
        <<"max_size">> => 10000,  % max size of storage (only for 'ram' and 'runtime'). 1-1000000.
        % for storage_mode = 'runtime'
        <<"expires_mode">> => <<"none">>, % if dms should automatically delete after expires. none | modify | create | custom. Select when to auto set (reset) timestamp in ts property.
        <<"expires_ttl_property">> => <<>>, % if dms should automatically delete after expires. name of property (int) containing initial ttl in seconds.
        <<"expires_ts_property">> => <<>>, % if dms should automatically delete after expires. name of property (long) for auto storing modify timestamp (milliseconds from 1970).
        % for storage_mode = 'category' (pg), 'history' (pg+partition), 'transactionlog' (kafka)
        <<"storage_instance">> => <<>>, % InstanceKey or #{Site => InstanceKey} (only for 'category', 'history', 'transactionlog'). Where to store data.
        <<"filestorage_instance">> => <<"master">>, % InstanceKey or #{Site => InstanceKey}. Where to store attachments.
        % for storage_mode = 'category', 'history', 'runtime'
        <<"lookup_properties">> => [], % list of property names for lookup operation (indexes, separate fields etc).
        % for storage_mode = 'history' (pg+partition) and 'transactionlog' (for clickhouse, not kafka)
        <<"partition_property">> => <<>>, % property of type=datetime to make partitioned history storage. For 'transactionlog' could be empty, then clickhouse sharding used automatically hash of 'id'.
        <<"partition_interval">> => <<"month">>, % size of partition in partitioned history storage.
        % for storage_mode = 'transactionlog' (for kafka only, clickhouse's shards and replicas are configured in its config files).
        <<"partition_count">> => 2, % how many partitions does topic have (1-10). This parameter directly passed to kafka.
        <<"replication_factor">> => 2, % how many replicas does topic have (1-4). This parameter directly passed to kafka.
        % for storage_mode = 'transactionlog'
        <<"notify_transactions">> => false, % as default subscribers are not notified by 'transactionlog' changes. But notification could be enabled.
        % for cache_mode = 'temp'
        <<"cache_sec">> => 600, % how long temporarily cache holds modified items. 1-infinity
        <<"cache_limit">> => 1000 % how many modified items are holded in temporarily cache. 1-infinity
    }.

%% @private
check_opts(Domain,Operation,Opts,Item) ->
    try
        CN = maps:get(<<"classname">>,Item),
        StorageMode = maps:get(<<"storage_mode">>,Item),
        Props = maps:get(<<"properties">>,Item),
        PropNames = [<<"id">>|[maps:get(<<"name">>,Prop) || Prop <- Props]],
        StType = case StorageMode of
                     <<"transactionlog">> -> <<"kafka">>;
                     <<"category">> -> <<"postgresql">>;
                     <<"history">> -> <<"postgresql">>;
                     _ -> undefined
                 end,
        maps:fold(fun(_K,_V,Acc) when Operation=='fixture' -> Acc;
                     (<<"comment">>,_V,Acc) -> Acc;
                     (<<"title">>,_V,Acc) -> Acc;
                     (<<"storage_instance">>,?MasterStorageInstance,Acc) -> Acc;
                     (<<"storage_instance">>=K,V,Acc) when StorageMode==<<"category">>; StorageMode==<<"history">>; StorageMode==<<"transactionlog">> ->
                            % sites
                            AllSites = [?PCFG:get_current_site()],
                            FSite = fun(Site) -> lists:member(Site,AllSites) end,
                            % storage instances
                            Storages = case ?DMS_CACHE:read_cache(Domain,?StoragesCN,#{},auto) of
                                           {ok,StorageItems,_} -> StorageItems;
                                           T ->
                                               ?LOG('$error',"Validator read storages: ~120tp",[T]),
                                               throw({error,{internal_error,<<"Validator cannot read storages">>}})
                                       end,
                            FInstance = fun(N) -> lists:foldl(fun(_,true) -> true;
                                                                 (Storage,false) ->
                                                                      maps:get(<<"type">>,Storage)==StType andalso maps:get(<<"instance">>,Storage,undefined)==N
                                                              end, false, Storages) end,
                            case V of
                                <<>> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance",[K,K])}});
                                _ when is_binary(V) ->
                                    case FInstance(V) of
                                        true -> Acc;
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}})
                                    end;
                                _ when is_map(V) ->
                                    F = fun(_,_,{error,_}=Err) -> Err;
                                           (Site,Instance,_) ->
                                                case {FSite(Site), FInstance(Instance)} of
                                                    {true,true} -> ok;
                                                    {false,_} -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing site",[K,K])}};
                                                    {_,false} -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}}
                                                end end,
                                    case maps:fold(F, ok, V) of
                                        ok -> Acc;
                                        {error,_}=Err -> throw(Err)
                                    end;
                                _ when is_list(V) ->
                                    F = fun(_,{error,_}=Err) -> Err;
                                        (Elt,_) when not is_map(Elt) -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list objects, containing key 'site'",[K,K])}};
                                        (Elt,_) ->
                                            FCheckInstance = fun() ->
                                                case FInstance(maps:get(<<"value">>,Elt,undefined)) of
                                                    false -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}};
                                                    true -> ok
                                                end end,
                                            case maps:get(<<"site">>,Elt,undefined) of
                                                undefined -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list objects, containing key 'site'",[K,K])}};
                                                <<"/reg/",_Reg/binary>> -> FCheckInstance();
                                                Site ->
                                                    case FSite(Site) of
                                                        false -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing site",[K,K])}};
                                                        true -> FCheckInstance()
                                                    end end end,
                                    case lists:foldl(F, false, V) of
                                        ok -> Acc;
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected non-empty list of per-site data",[K,K])}});
                                        {error,_}=Err -> throw(Err)
                                    end;
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected string or object",[K,K])}})
                            end;
                     (<<"filestorage_instance">>,<<"master">>,Acc) -> Acc;
                     (<<"filestorage_instance">>=K,V,Acc) ->
                            % sites
                            AllSites = [?PCFG:get_current_site()],
                            FSite = fun(Site) -> lists:member(Site,AllSites) end,
                            % storage instances
                            Storages = case ?DMS_CACHE:read_cache(Domain,?StoragesCN,#{},auto) of
                                           {ok,StorageItems,_} -> StorageItems;
                                           T ->
                                               ?LOG('$error',"Validator read storages: ~120tp",[T]),
                                               throw({error,{internal_error,<<"Validator cannot read storages">>}})
                                       end,
                            FInstance = fun(N) -> lists:foldl(fun(_,true) -> true;
                                                                 (Storage,false) ->
                                                                     lists:member(maps:get(<<"type">>,Storage), [<<"s3">>,<<"nfs">>,<<"fs">>,<<"fsync">>]) andalso maps:get(<<"instance">>,Storage,undefined)==N
                                                              end, false, Storages) end,
                            case V of
                                <<>> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance",[K,K])}});
                                _ when is_binary(V) ->
                                    case FInstance(V) of
                                        true -> Acc;
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}})
                                    end;
                                _ when is_map(V) ->
                                    F = fun(_,_,{error,_}=Err) -> Err;
                                           (Site,Instance,_) ->
                                                case {FSite(Site), FInstance(Instance)} of
                                                    {true,true} -> ok;
                                                    {false,_} -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing site",[K,K])}};
                                                    {_,false} -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}}
                                                end end,
                                    case maps:fold(F, ok, V) of
                                        ok -> Acc;
                                        {error,_}=Err -> throw(Err)
                                    end;
                                _ when is_list(V) ->
                                    F = fun(_,{error,_}=Err) -> Err;
                                           (Elt,_) when not is_map(Elt) -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list objects, containing key 'site'",[K,K])}};
                                           (Elt,_) ->
                                               FCheckInstance = fun() ->
                                                                    case FInstance(maps:get(<<"value">>,Elt,undefined)) of
                                                                        false -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing storage instance of type '~ts'",[K,K,StType])}};
                                                                        true -> ok
                                                                    end end,
                                               case maps:get(<<"site">>,Elt,undefined) of
                                                   undefined -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list objects, containing key 'site'",[K,K])}};
                                                   <<"/reg/",_Reg/binary>> -> FCheckInstance();
                                                   Site ->
                                                       case FSite(Site) of
                                                           false -> {error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected existing site",[K,K])}};
                                                           true -> FCheckInstance()
                                                       end end end,
                                    case lists:foldl(F, false, V) of
                                        ok -> Acc;
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected non-empty list of per-site data",[K,K])}});
                                        {error,_}=Err -> throw(Err)
                                    end;
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected string or object",[K,K])}})
                            end;
                     (<<"max_limit">>=K,V,Acc) ->
                            Limit = case ?BU:to_int(V,undefined) of
                                        L when L>=10, L=<10000 -> L;
                                        _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected 10 .. 10000",[K,K])}})
                                    end,
                            Acc#{K:=Limit};
                     (<<"max_mask">>=K,V,Acc) ->
                            Mask = case V of
                                       [] -> V;
                                       L when is_list(L) ->
                                           case V -- PropNames of
                                               [] -> V;
                                               [_|_] -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list of existing properties or empty list",[K,K])}})
                                           end;
                                       _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list of existing properties or empty list",[K,K])}})
                                   end,
                            Acc#{K:=Mask};
                     (<<"max_size">>=K,V,Acc) when StorageMode==<<"ram">>; StorageMode==<<"runtime">> ->
                            case ?BU:to_int(V,10000) of
                                V1 when V1>0, V1=<1000000 -> Acc#{K:=V1};
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected value less than 1000000 for storage_mode='~ts'",[K,K,StorageMode])}})
                            end;
                     (<<"expires_mode">>=K,V,Acc) ->
                            case lists:member(StorageMode,[<<"runtime">>]) of
                                false ->
                                    case V of
                                        <<"none">> -> Acc;
                                        _ -> throw({error,{invalid_params,?BU:strbin("field=opts|Invalid 'opts.~ts'. Expected 'none' for storage_mode='~ts'", [K,StorageMode])}})
                                    end;
                                true ->
                                    Available = [<<"none">>,<<"modify">>,<<"create">>,<<"custom">>],
                                    case lists:member(V,Available) of
                                        true -> Acc;
                                        false -> throw({error,{invalid_params,?BU:strbin("field=opts|Invalid 'opts.~ts'. Expected ~ts", [K,?BU:join_binary_quoted(Available,<<"'">>,<<", ">>)])}})
                                    end
                            end;
                     (<<"expires_ttl_property">>=K,V,Acc) ->
                            case maps:get(<<"expires_mode">>,Acc) of
                                <<"none">> -> Acc;
                                _ when V==<<>> -> throw({error,{invalid_params,?BU:strbin("field=opts|Invalid 'opts.~ts'. Expected non-empty value containing existing property name", [K])}});
                                _ ->
                                    Res = lists:foldl(fun(_,{ok,_}=Ok) -> Ok;
                                                         (Prop,false) -> case maps:get(<<"name">>,Prop,undefined) of N when N==V -> {ok,Prop}; _ -> false end
                                                      end, false, Props),
                                    case Res of
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Property not found",[K,K])}});
                                        {ok,#{<<"data_type">> := DataType}} when DataType==<<"integer">> -> Acc;
                                        {ok,_} -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected data_type of 'integer'",[K,K])}})
                                    end end;
                     (<<"expires_ts_property">>=K,V,Acc) ->
                            case maps:get(<<"expires_mode">>,Acc) of
                                <<"none">> -> Acc;
                                _ when V==<<>> -> throw({error,{invalid_params,?BU:strbin("field=opts|Invalid 'opts.~ts'. Expected non-empty value containing existing property name", [K])}});
                                _ ->
                                    case lists:foldl(fun(_,{ok,_}=Ok) -> Ok;
                                        (Prop,false) -> case maps:get(<<"name">>,Prop,undefined) of N when N==V -> {ok,Prop}; _ -> false end end, false, Props) of
                                        false -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Property not found",[K,K])}});
                                        {ok,#{<<"data_type">> := DataType}} when DataType==<<"long">> -> Acc;
                                        {ok,_} -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected data_type of 'long'",[K,K])}})
                                    end end;
                     (<<"lookup_properties">>=K,V,Acc) ->
                            List = case V of
                                       [] -> V;
                                       L when is_list(L) ->
                                           case V -- PropNames of
                                               [] -> V;
                                               [_|_] -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list of existing properties or empty list",[K,K])}})
                                           end;
                                       _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected list of existing properties or empty list",[K,K])}})
                                   end,
                            Acc#{K:=List};
                     (<<"partition_property">>=K,V,Acc) ->
                            case V of
                                _ when StorageMode==<<"abstract">> -> Acc;
                                <<>> when StorageMode==<<"history">> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected property of data_type='datetime'",[K,K])}});
                                <<>> when StorageMode==<<"transactionlog">> -> Acc#{K:=?BU:to_binary(V)};
                                _ when StorageMode==<<"history">>;StorageMode==<<"transactionlog">> ->
                                    PropsT = [{maps:get(<<"name">>,Prop),Prop} || Prop <- Props],
                                    case ?BU:get_by_key(?BU:to_binary(V),PropsT,undefined) of
                                        undefined -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected name of existing property",[K,K])}});
                                        Prop ->
                                            case maps:get(<<"data_type">>,Prop) of
                                                <<"datetime">> -> Acc#{K:=?BU:to_binary(V)};
                                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected property of data_type='datetime'",[K,K])}})
                                            end end;
                                <<>> -> Acc#{K:=?BU:to_binary(V)};
                                _ when StorageMode/=<<"history">> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected nonempty only for storage_mode='history' and 'transactionlog'",[K,K])}})
                            end;
                     (<<"partition_interval">>=K,V,Acc) ->
                            PartitionProp = maps:get(<<"partition_property">>,Acc),
                            case StorageMode of
                                <<"history">> when V==<<"day">>; V==<<"month">>; V==<<"year">> -> Acc;
                                <<"history">> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected 'day', 'month', 'year'",[K,K])}});
                                <<"transactionlog">> when V==<<"day">>; V==<<"month">>; V==<<"year">> -> Acc;
                                <<"transactionlog">> when PartitionProp==<<>> -> Acc;
                                <<"transactionlog">> -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected 'day', 'month', 'year' when partition_property is defined",[K,K])}});
                                _ -> Acc
                            end;
                     (<<"partition_count">>=K,V,Acc) ->
                            case ?BU:to_int(V,0) of
                                V1 when V1>=1 -> Acc#{K:=V1};
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected positive integer",[K,K])}})
                            end;
                     (<<"replication_factor">>=K,V,Acc) ->
                            case ?BU:to_int(V,0) of
                                V1 when V1>=1 -> Acc#{K:=V1};
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected positive integer",[K,K])}})
                            end;
                     (<<"notify_transactions">>=K,V,Acc) -> Acc#{K:=?BU:to_bool(V)};
                     (<<"cache_sec">>=K,V,Acc) ->
                            case ?BU:to_int(V) of
                                V1 when V1>=1 -> Acc#{K:=V1};
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected positive integer",[K,K])}})
                            end;
                     (<<"cache_limit">>=K,V,Acc) ->
                            case ?BU:to_int(V) of
                                V1 when V1>=1 -> Acc#{K:=V1};
                                _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected positive integer",[K,K])}})
                            end;
                     (<<"notify">>=K,V,Acc) -> Acc#{K:=?BU:to_bool(V)};
                     (<<"check_required_fill_defaults">>=K,V,Acc) -> Acc#{K:=?BU:to_bool(V)};
                     (<<"replace_without_read">>=K,V,Acc) ->
                         V1 = ?BU:to_bool(V),
                         case maps:get(<<"store_changehistory_mode">>,Acc,<<"none">>) of
                             <<"none">> -> Acc#{K:=V1};
                             _ when V1 -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected false when 'store_changehistory_mode' is not 'none'",[K,K])}});
                             _ -> Acc#{K:=V1}
                         end;
                     (<<"notify_integrity_timeout">>=K,V,Acc) -> Acc#{K:=erlang:min(60000,erlang:max(0,?BU:to_int(V,0)))};
                     (<<"store_changehistory_mode">>=K,V,Acc) ->
                         case V of
                             <<"none">> -> Acc;
                             _ when StorageMode==<<"history">>; StorageMode==<<"transactionlog">> ->
                                 throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected: 'none' for storage_mode='~ts'",[K,K,StorageMode])}});
                             _ when CN==?ModelEventsCN ->
                                 throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected: 'none' for classname='~ts'",[K,K,?ModelEventsCN])}});
                             _ when V==<<"sync">> -> Acc;
                             _ -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected: 'none' | 'sync'",[K,K])}})
                         end;
                     (<<"validator">>=K,V,Acc) ->
                         case V==<<>> orelse ?GN_REG:whereis_name(V)/=undefined of
                             false when Operation/='fixture' -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected global name of existing service",[K,K])}});
                             _ -> Acc
                         end;
                     (<<"appserver">>=K,V,Acc) ->
                         case V==<<>> orelse ?GN_REG:whereis_name(V)/=undefined of
                             false when Operation/='fixture' -> throw({error,{invalid_params, ?BU:strbin("field=opts.~s|Invalid 'opts'.'~s'. Expected global name of existing service",[K,K])}});
                             _ -> Acc
                         end;
                     (_K,_V,Acc) -> Acc
                  end, Opts, Opts)
    catch
        error:R:ST ->
            ?LOG('$crash',"Validator crashed on check_opts: ~120tp~n\tItem: ~160tp~n\tStack: ~160tp",[{error,R},Item,ST]),
            {error, {invalid_params, <<"field=opts|Invalid 'opts'. Could not parse">>}};
        throw:{error,_}=Err -> Err
    end.

%% -------------------------------------
%% Check all classes in complex
%% -------------------------------------
validate_all_classes(Domain,Operation,{_E0,Item}) ->
    Id = maps:get(<<"id">>,Item),
    CN = maps:get(<<"classname">>,Item),
    AllClasses = case ?DMS_CACHE:read_cache(Domain,?ClassesCN,#{},auto) of
                     {ok,ClassItems,_} -> lists:filter(fun(ClassItem) -> maps:get(<<"id">>,ClassItem) /= Id end, ClassItems);
                     T ->
                         ?LOG('$error',"Validator read classes: ~120tp",[T]),
                         {error,{internal_error,<<"Validator cannot read classes">>}}
                 end,
    case Operation of
        'delete' -> check_nesting_tree('down',Item,AllClasses);
        O when O=='create';O=='replace';O=='update';O=='delete' ->
            case lists:filter(fun(ClassItem) -> maps:get(<<"classname">>,ClassItem) == CN end, AllClasses) of
                [_|_] -> {error, {invalid_params, <<"field=classname|classname already exists">>}};
                [] -> check_nesting_tree('up',Item,AllClasses)
            end end.

%% -------------------------------------
%% Check nesting tree. If parent exists, if nesting tree is not cycled
%% -------------------------------------
check_nesting_tree(up,Item,AllClasses) ->
    Id = maps:get(<<"id">>,Item),
    EmptyId = ?BU:emptyid(),
    F = fun F(ItemX,Level) ->
                Find = fun(IdClassX) -> lists:filter(fun(ClassItem) -> maps:get(<<"id">>,ClassItem) == IdClassX end, AllClasses) end,
                case maps:get(<<"parent_id">>,ItemX) of
                    EmptyId -> ok;
                    IdClass ->
                        case Find(IdClass) of
                            [] when Level==0 ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Expected id of existing class or empty">>}};
                            [] when Level>0 ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Nesting tree is broken">>}};
                            [#{<<"id">> := ParentId}] when ParentId==Id ->
                                {error, {invalid_params, <<"field=parent_id|Invalid parent_id. Nesting cycle detected">>}};
                            [ParentItem] -> F(ParentItem,Level+1)
                        end end end,
    F(Item,0);
%%
check_nesting_tree(down,Item,AllClasses) ->
    Id = maps:get(id,Item),
    F = fun(V, {N,L}=Acc) ->
               K = maps:get(<<"classname">>,V),
               % check nested classes
               case maps:get(<<"parent_id">>,V) of
                   ParentId when ParentId==Id -> {[K|N],L};
                   _ ->
                       % check classes linked by properties
                       F = fun(Prop) ->
                               case maps:get(<<"data_type">>,Prop) of
                                   <<"entity">> -> maps:get(<<"idclass">>,Prop) == Id;
                                   _ -> false
                               end end,
                       case lists:any(F,maps:get(<<"properties">>,V)) of
                           true -> {N,[K|L]};
                           false -> Acc
                       end end end,
    case lists:foldl(F,{[],[]},AllClasses) of
        {[],[]} -> ok;
        {Nests,[]} -> {error,{invalid_operation,?BU:strbin("Class is nested by ~ts", [?BU:join_binary_quoted(Nests,<<"'">>,<<",">>)])}};
        {[],Links} -> {error,{invalid_operation,?BU:strbin("Class is linked by ~ts", [?BU:join_binary_quoted(Links,<<"'">>,<<",">>)])}};
        {Nests,Links} ->
            {error,{invalid_operation,?BU:strbin("Class is nested by ~ts; linked by ~ts", [?BU:join_binary_quoted(Nests,<<"'">>,<<",">>),?BU:join_binary_quoted(Links,<<"'">>,<<",">>)])}}
    end.