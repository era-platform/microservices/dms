%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.05.2021
%%% @doc Validates 'storages' entity on any modify operation.

-module(dms_validator_coll_storages).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([validate/4,
         decorate_fixture/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Validate storage entity on any modify operation
%% --------------------------------------
-spec validate(Domain::binary() | atom(),
               Operation :: create | replace | update | delete,
               EntityInfo::{Id::binary(),E0::map(),E1::map()} | undefined,
               ModifierInfo::{Type::atom(),Id::binary()} | undefined) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
validate(Domain,Operation,{_Id,E0,E1}=EntityInfo,ModifierInfo) ->
    FunIsFixt = fun(E) -> E /= undefined andalso maps:get(<<"instance">>,E,undefined) == ?MasterStorageInstance end,
    IsFixtureEntity = FunIsFixt(E0) orelse FunIsFixt(E1),
    case IsFixtureEntity of
        true when Operation=='delete', ModifierInfo /= {'fixturer',?StoragesCN} ->
            {error,{invalid_operation,<<"Access denied. Cannot remove 'master' storage instance directly. Modify configuration instead.">>}};
        _ when Operation=='delete' ->
            {ok,undefined};
        true when Operation=='create'; Operation=='replace'; Operation=='update' ->
            check_fixture(Domain,Operation,EntityInfo,ModifierInfo);
        _ ->
            case check_entity(Domain,Operation,E1) of
                {error,_}=Err -> Err;
                {ok,EntityV} ->
                    case validate_all_storages(Domain,EntityV) of
                        ok -> {ok,EntityV};
                        {error,_}=Err -> Err
                    end end end.

%% --------------------------------------
%% Helps to format and decorate fixture
%% --------------------------------------
-spec decorate_fixture(Domain::binary() | atom(), Entity::map()) -> {ok,Entity::map()} | {error,Reason::term()}.
%% --------------------------------------
decorate_fixture(Domain,Entity) ->
    case check_entity(Domain,'fixture',Entity) of
        {error,_}=Err -> Err;
        {ok,EntityV} -> {ok,EntityV}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Check if master
%% -------------------------------------
check_fixture(Domain,_Operation,{_Id,_E0,E1},ModifierInfo) ->
    Fixtures = ?FixtureStorages:master_storages(Domain),
    Keys = [<<"id">>,<<"type">>,<<"instance">>,<<"priority">>,<<"params">>],
    case lists:member(maps:with(Keys,E1),Fixtures) of
        false when ModifierInfo /= {fixturer,?StoragesCN} ->
            % TODO: check E1 to fixture, but no modifier_info
            {error,{invalid_operation,<<"Access denied. Cannot modify 'master' storage instance directly. Modify configuration instead.">>}};
        _ -> {ok,E1}
    end.

%% -------------------------------------
%% Check storage properties
%% -------------------------------------
check_entity(Domain,Operation,Item) ->
    check_1(Domain,Operation,Item).

%% @private
%% type
check_1(Domain,Operation,Item) ->
    T = maps:get(<<"type">>,Item),
    case T of
        <<"postgresql">> -> check_2(T,Domain,Operation,Item);
        <<"kafka">> -> check_2(T,Domain,Operation,Item);
        <<"clickhouse">> -> check_2(T,Domain,Operation,Item);
        <<"s3">> -> check_2(T,Domain,Operation,Item);
        <<"nfs">> -> check_2(T,Domain,Operation,Item);
        <<"fs">> -> check_2(T,Domain,Operation,Item);
        <<"fsync">> -> check_2(T,Domain,Operation,Item);
        _ -> {error, {invalid_params, ?BU:strbin("field=type|Invalid 'type'. Expected: 'postgresql', 'kafka', 'clickhouse', 's3', 'nfs', 'fs', 'fsync'", [])}}
    end.
%% instance
check_2(Type,Domain,Operation,Item) ->
    case maps:get(<<"instance">>,Item,<<>>) of
        <<>> -> {error, {invalid_params, ?BU:strbin("field=instance|Invalid 'instance'. Expected non-empty value", [])}};
        Instance when is_binary(Instance) -> check_3(Type,Domain,Operation,Item);
        _ -> {error, {invalid_params, ?BU:strbin("field=instance|Invalid 'instance'. Expected non-empty string value", [])}}
    end.
%% params
check_3(Type,Domain,Operation,Item) ->
    case maps:get(<<"params">>,Item) of
        Term when not is_map(Term) -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected object to define type-specific params (ex. host, port, login, pwd, database)", [])}};
        StMap when is_map(StMap) ->
            case catch validate_storage(Type,StMap) of
                {ok,StMap1} -> check_x(Domain,Operation,Item#{<<"params">> => StMap1});
                {error,_}=Err -> Err;
                {'EXIT',R} -> throw(R)
            end end.
%% final
check_x(_Domain,_Operation,Item) -> {ok,Item}.

%% -------------------------------------
%% Check storage type params
%% -------------------------------------
validate_storage(<<"postgresql">>, StMap) ->
    ReqKeys = [<<"host">>,<<"port">>,<<"login">>,<<"pwd">>,<<"database">>],
    OptKeys = [<<"replication_controller">>],
    ItemKeys = maps:keys(StMap),
    MissedKeys = ReqKeys--ItemKeys,
    UnknownKeys = (ItemKeys--[<<"type">>])--(ReqKeys++OptKeys),
    if
        MissedKeys/=[] ->
            MissF = ?BU:to_list(?BU:join_binary(MissedKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missed required fields: ~ts",[MissF])}});
        UnknownKeys/=[] ->
            UnknF = ?BU:to_list(?BU:join_binary(UnknownKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Unknown keys: ~ts",[UnknF])}});
        true -> true
    end,
    F = fun(<<"port">>,V) when is_integer(V), V>0, V<65536 -> V;
           (<<"port">> =K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of '~ts' must be integer in [1,65535]",[K])}});
           (_,V) when is_binary(V) -> V;
           (K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of '~ts' must be string",[K])}})
        end,
    {ok,maps:map(F,StMap)};
%%
validate_storage(<<"clickhouse">>, StMap) ->
    ReqKeys = [<<"host">>,<<"login">>,<<"password">>],
    OptKeys = [<<"port">>,<<"cluster">>],
    ItemKeys = maps:keys(StMap),
    MissedKeys = ReqKeys--ItemKeys,
    UnknownKeys = (ItemKeys--[<<"type">>])--(ReqKeys++OptKeys),
    if
        MissedKeys/=[] ->
            MissF = ?BU:to_list(?BU:join_binary(MissedKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missed required fields: ~ts",[MissF])}});
        UnknownKeys/=[] ->
            UnknF = ?BU:to_list(?BU:join_binary(UnknownKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Unknown keys: ~ts",[UnknF])}});
        true -> true
    end,
    F = fun(<<"port">>,V) when is_integer(V), V>0, V<65536 -> V;
           (<<"port">> =K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of '~ts' must be integer in [1,65535]",[K])}});
           (_,V) when is_binary(V) -> V;
           (K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of '~ts' must be string",[K])}})
        end,
    {ok,maps:map(F,StMap)};
%%
validate_storage(<<"kafka">>, StMap) ->
    ReqKeys = [<<"endpoints">>],
    OptKeys = [<<"default_reconnect_cool_down_seconds">>,<<"partitioner">>],
    ItemKeys = maps:keys(StMap),
    MissedKeys = ReqKeys--ItemKeys,
    UnknownKeys = (ItemKeys--[<<"type">>])--(ReqKeys++OptKeys),
    if
        MissedKeys/=[] ->
            MissF = ?BU:to_list(?BU:join_binary(MissedKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missed required fields: ~ts",[MissF])}});
        UnknownKeys/=[] ->
            UnknF = ?BU:to_list(?BU:join_binary(UnknownKeys,<<"', '">>)),
            throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Unknown keys: ~ts",[UnknF])}});
        true -> true
    end,
    Fep = fun(EP) when not is_map(EP) ->
        throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected 'endpoints' a non-empty list of objects {host,port}",[])}});
        (EP) ->
            HostX = maps:get(<<"host">>,EP,undefined),
            PortX = ?BU:to_int(maps:get(<<"port">>,EP,undefined),0),
            case {HostX,PortX} of
                {H,_} when not is_binary(H) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected endpoint's 'host' value of type string",[])}});
                {_,P} when not is_integer(P) orelse P<1 orelse P>65535->
                    throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of 'port' must be integer in [1,65535]",[])}});
                {Host,Port} -> #{<<"host">> => Host, <<"port">> => Port}
            end end,
    F = fun(<<"endpoints">> =K,[]) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected '~ts' a non-empty list of objects {host,port}",[K])}});
           (<<"endpoints">>,V) when is_list(V) -> lists:map(Fep,V);
           (<<"endpoints">> =K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected '~ts' a non-empty list of objects {host,port}",[K])}});
           (_,V) when is_binary(V) -> V;
           (K,_) -> throw({error,{invalid_params, ?BU:strbin("field=params|Invalid 'params'. Value of '~ts' must be string",[K])}})
        end,
    {ok,maps:map(F,StMap)};
%%
validate_storage(<<"s3">> = Type, StMap) ->
    Storage = #{<<"type">> => Type},
    ReqKeys = [<<"bucket">>,<<"key">>,<<"secret">>],
    OptKeys = [<<"region">>,<<"endpoint">>,<<"prefix">>],
    St1 = lists:foldl(fun(_, {error,_}=ErrAcc) -> ErrAcc;
                         (Key, Acc) ->
                             case maps:get(Key, StMap, undefined) of
                                 undefined ->
                                     case lists:member(Key, ReqKeys) of
                                         true -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missing '~s' key",[Key])}};
                                         false -> Acc
                                     end;
                                 V when is_binary(V); is_integer(V); is_atom(V) -> Acc#{Key => ?BU:to_binary(V)};
                                 _ -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Wrong '~s' value",[Key])}}
                             end
                      end, Storage, ReqKeys ++ OptKeys),
    case St1 of
        StMapNorm when is_map(StMapNorm) -> {ok, StMapNorm};
        {error,_}=Err -> Err
    end;
%%
validate_storage(<<"nfs">> = Type, StMap) ->
    % TODO: check configkey in real config
    Storage = #{<<"type">> => Type},
    ReqKeys = [<<"configkey">>],
    OptKeys = [<<"prefix">>],
    St1 = lists:foldl(fun(_, {error,_}=ErrAcc) -> ErrAcc;
                         (Key, Acc) ->
                             case maps:get(Key, StMap, undefined) of
                                 undefined ->
                                     case lists:member(Key, ReqKeys) of
                                         true -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missing '~s' key",[Key])}};
                                         false -> Acc
                                     end;
                                 V when is_binary(V); is_integer(V); is_atom(V) -> Acc#{Key => ?BU:to_binary(V)};
                                 _ -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Wrong '~s' value",[Key])}}
                             end
                      end, Storage, ReqKeys ++ OptKeys),
    case St1 of
        StMapNorm when is_map(StMapNorm) -> {ok, StMapNorm};
        {error,_}=Err -> Err
    end;
%%
validate_storage(<<"fs">> = Type, StMap) ->
    % TODO: check configkey in real config
    Storage = #{<<"type">> => Type},
    ReqKeys = [<<"configkey">>],
    OptKeys = [<<"prefix">>],
    St1 = lists:foldl(fun(_, {error,_}=ErrAcc) -> ErrAcc;
                         (Key, Acc) ->
                             case maps:get(Key, StMap, undefined) of
                                 undefined ->
                                     case lists:member(Key, ReqKeys) of
                                         true -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missing '~s' key",[Key])}};
                                         false -> Acc
                                     end;
                                 V when is_binary(V); is_integer(V); is_atom(V) -> Acc#{Key => ?BU:to_binary(V)};
                                 _ -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Wrong '~s' value",[Key])}}
                             end
                      end, Storage, ReqKeys ++ OptKeys),
    case St1 of
        StMapNorm when is_map(StMapNorm) -> {ok, StMapNorm};
        {error,_}=Err -> Err
    end;

%%
validate_storage(<<"fsync">> = Type, StMap) ->
    % TODO: check configkey in real config
    Storage = #{<<"type">> => Type},
    ReqKeys = [<<"prefix">>],
    OptKeys = [],
    St1 = lists:foldl(fun(_, {error,_}=ErrAcc) -> ErrAcc;
                         (Key, Acc) ->
                            case maps:get(Key, StMap, undefined) of
                                undefined ->
                                    case lists:member(Key, ReqKeys) of
                                        true -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Missing '~s' key",[Key])}};
                                        false -> Acc
                                    end;
                                V when is_binary(V); is_integer(V); is_atom(V) -> Acc#{Key => ?BU:to_binary(V)};
                                _ -> {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Wrong '~s' value",[Key])}}
                            end
                      end, Storage, ReqKeys ++ OptKeys),
    case St1 of
        StMapNorm when is_map(StMapNorm) -> {ok, StMapNorm};
        {error,_}=Err -> Err
    end.

%% -------------------------------------
%% Check all storages in complex
%% Check all storages setting for uniqueness (only for create and update)
%% -------------------------------------
validate_all_storages(Domain,Entity) ->
    Id = maps:get(<<"id">>,Entity),
    UpItems = case ?DMS_CACHE:read_cache(Domain,?StoragesCN,#{},auto) of
                  {ok,Items,_} -> lists:filter(fun(Item) -> maps:get(<<"id">>,Item) /= Id end, Items);
                  T ->
                      ?LOG('$error',"Validator read storages: ~120tp",[T]),
                      {error,{internal_error,<<"Validator cannot read storages">>}}
              end,
    validate_groups(Domain,UpItems++[Entity]).

%% @private
validate_groups(_Domain,UpItems) ->
    lists:foldl(fun(Type,ok) ->
                    case validate_group(Type,lists:filter(fun(Item) -> maps:get(<<"type">>,Item)==Type end, UpItems)) of
                        ok -> ok;
                        {error,_}=Err -> Err
                    end;
                    (_,Acc) -> Acc
                end, ok, [<<"postgresql">>,<<"kafka">>,<<"clickhouse">>,<<"s3">>,<<"nfs">>,<<"fs">>,<<"fsync">>]).

%% postgresql - every instance has same database and different host:port
validate_group(<<"postgresql">>=Type,Items) ->
    Fun = fun(Inst) ->
               fun(_,{error,_}=Acc) -> Acc;
                  (Params,{ok,Acc,LP0,DB0}) ->
                        HP = {maps:get(<<"host">>,Params), maps:get(<<"port">>,Params)},
                        LP = {maps:get(<<"login">>,Params), maps:get(<<"pwd">>,Params)},
                        DB = maps:get(<<"database">>,Params),
                        case maps:is_key(HP,Acc) of
                            true ->
                                {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected different 'host:port' for all samples of type='~ts' and instance='~ts'.",[Type,Inst])}};
                            false when DB0==undefined,LP0==undefined -> {ok,Acc#{HP=>true},LP,DB};
                            false when DB0/=DB ->
                                {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected same 'database' for all samples of type='~ts' and instance='~ts'.",[Type,Inst])}};
                            false when LP0/=LP ->
                                {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected same 'login:pwd' for all samples of type='~ts' and instance='~ts'.",[Type,Inst])}};
                            false -> {ok,Acc#{HP=>true},LP,DB}
                        end end end,
    maps:fold(fun(Inst,ParamsL,ok) -> case lists:foldl(Fun(Inst),{ok,#{},undefined,undefined},ParamsL) of {error,_}=Err -> Err; {ok,_,_} -> ok end;
                 (_,_,Acc) -> Acc
              end, ok, by_instance(Items));
%% clickhouse - every instance has same cluster and different host:port
validate_group(<<"clickhouse">>=Type,Items) ->
    Fun = fun(Inst) ->
               fun(_,{error,_}=Acc) -> Acc;
                  (Params,{ok,Acc,Cluster0}) ->
                    HP = {maps:get(<<"host">>,Params), maps:get(<<"port">>,Params)},
                    Cluster = maps:get(<<"cluster">>,Params),
                    case maps:is_key(HP,Acc) of
                        true ->
                            {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected different Host:Port for all samples of type='~ts' and instance='~ts'.",[Type,Inst])}};
                        false when Cluster0==undefined -> {ok,Acc#{HP=>true},Cluster};
                        false when Cluster0==Cluster -> {ok,Acc#{HP=>true},Cluster};
                        false when Cluster0/=Cluster ->
                            {error, {invalid_params, ?BU:strbin("field=params|Invalid 'params'. Expected same 'cluster' for all samples of type='~ts' and instance='~ts'.",[Type,Inst])}}
                    end end end,
    maps:fold(fun(Inst,ParamsL,ok) -> case lists:foldl(Fun(Inst),{ok,#{},undefined},ParamsL) of {error,_}=Err -> Err; {ok,_,_} -> ok end;
                 (_,_,Acc) -> Acc
              end, ok, by_instance(Items));
%% others - no limits
validate_group(_,_) -> ok.

%% @private
by_instance(Items) ->
    lists:foldl(fun(Item,Acc) ->
                        Inst=maps:get(<<"instance">>,Item,<<>>),
                        Acc#{Inst => [maps:get(<<"params">>,Item) | maps:get(Inst,Acc,[])]}
                end,#{},Items).