%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc Callbacks for dmlib

-module(dms_dmlib_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_storages/3,
         notify_function/7,
         drop_subscriptions/2,
         store_changehistory_function/6,
         validate_function/6]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(Notify, dms_notify).
-define(NotifyHelper, dms_notify_helper).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% -----------------------------------------------------------
%% Callback request for storages of selected type and instance
%% -----------------------------------------------------------
-spec get_storages(Domain::binary() | atom(), StorageType :: binary(), StorageInstance::binary() | map()) ->
    {ok,[Item::map()],HC::term()} | {error,Reason::term()}.
%% -----------------------------------------------------------
get_storages(Domain, ?MasterStorageType, ?MasterStorageInstance) ->
    FixtureStorages = lists:filter(fun(StorageItem) -> maps:get(<<"type">>,StorageItem)==?MasterStorageType end, ?FixtureStorages:master_storages(Domain)),
    Err = {error,{internal_error,<<"Invalid 'master_dbstrings' in domain entity of parent domain. Expected list of connection strings or existing aliases (keys: host,port,login,pwd,database)">>}},
    get_storages_params(FixtureStorages,[],Err);

%%
get_storages(Domain,StorageType,StorageInstance) ->
    CurSite = ?PCFG:get_current_site(),
    case ?U:get_value_for_site(CurSite,StorageInstance) of
        {ok,InstanceKey} when is_binary(InstanceKey) ->
            FixtureStorages = fixture_storages(Domain,StorageType),
            Err = {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing storage_instance (opts.storage_instance should match at least 1 domain's storage of type: '~ts'",[StorageType])}},
            Filter = [<<"&&">>,[<<"==">>,[<<"property">>,<<"type">>],StorageType],[<<"==">>,[<<"property">>,<<"instance">>],InstanceKey]],
            ReadOpts = #{<<"filter">> => Filter,<<"order">> => [<<"priority">>,<<"id">>]},
            case ?DMS_CACHE:read_cache(Domain,<<"storages">>,ReadOpts,auto) of
                {error,_}=Err1 when FixtureStorages==[] -> Err1;
                {error,_}=Err1 ->
                    % HERE IS START LOAD POINT OF DOMAIN
                    % When no storages collection is found, fixtured collections of 'storages' and 'classes' should be loaded and setup
                    ?LOG('$warning',"Read storages '~ts' in '~ts': ~120tp",[StorageType,Domain,Err]),
                    get_storages_params(FixtureStorages,[],Err1);
                {ok,StorageItems,_} ->
                    get_storages_params(FixtureStorages,StorageItems,Err)
            end;
        false -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing opts.storage_instance for site '~ts'",[CurSite])}};
        _ -> {error,{internal_error,?BU:strbin("Invalid class metadata. Expected existing opts.storage_instance value for site '~ts'",[CurSite])}}
    end.

%% @private
fixture_storages(Domain,<<"postgresql">>) ->
    % HERE IS START LOAD POINT OF DOMAIN
    ?FixtureStorages:master_storages(Domain);
fixture_storages(_,_) -> [].

%% @private
get_storages_params(Fixtures,Items,DefaultErr) ->
    FixtureIds = lists:map(fun(Item) -> maps:get(<<"id">>,Item) end, Fixtures),
    FixtureInsts = lists:map(fun(Item) -> maps:get(<<"instance">>,Item) end, Fixtures),
    Items1 = lists:filter(fun(Item) ->
                                not lists:member(maps:get(<<"id">>,Item),FixtureIds)
                                    and not lists:member(maps:get(<<"instance">>,Item),FixtureInsts)
                          end, Items),
    case lists:map(fun(SI) -> maps:get(<<"params">>,SI) end, Fixtures++Items1) of
        [] -> DefaultErr;
        StoragesParams -> {ok,StoragesParams}
    end.


%% -----------------------------------------------------------
%% callback to notify
%% -----------------------------------------------------------
-spec notify_function(Domain :: binary() | atom(),
                      CN :: binary(),
                      ClassMeta :: map(),
                      Operation :: create | update | delete | clear | reload | corrupt,
                      EntityInfo :: {Id::undefined | binary(), E0 :: undefined | map(), E1 :: undefined | map()} | undefined,
                      ModifierInfo :: {Type::undefined | atom(),Id::undefined | binary()},
                      ExState :: term())
      -> NewExState::term().
%% -----------------------------------------------------------
notify_function(Domain, CN, ClassMeta, Operation, EntityInfo, ModifierInfo, ExState) ->
    ?Notify:notify(Domain,CN,ClassMeta,Operation,EntityInfo,ModifierInfo,ExState).


%% -----------------------------------------------------------
%% Callback to drop subscriptions for CN
%% -----------------------------------------------------------
-spec drop_subscriptions(Domain::binary() | atom(), CN::binary()) -> ok.
%% -----------------------------------------------------------
drop_subscriptions(Domain, CN) ->
    EventClass = <<"modelevents">>,
    EventType = <<"data_changed">>,
    EventName = <<EventClass/binary,".",EventType/binary>>,
    ?NotifyHelper:drop_subscriptions(Domain,EventName,CN).


%% -----------------------------------------------------------
%% Callback to validate
%% -----------------------------------------------------------
-spec validate_function(Domain::binary() | atom(),
                        Validator :: term(),
                        CN :: binary(),
                        Operation :: create | update | delete,
                        EntityInfo :: {Id::undefined | binary(), E0 :: undefined | map(), E1 :: undefined | map()},
                        ModifierInfo :: {Type::undefined | atom(),Id::undefined | binary()}) ->
    {ok,Entity::map()} | {error,Reason::term()}.
%% ----------------------------------------------
validate_function(Domain, Validator, CN, Operation, EntityInfo, ModifierInfo) ->
    do_validate(Domain,Validator,CN,Operation,EntityInfo,ModifierInfo,3,undefined).

%% @private
do_validate(_, _, _, _, _, _, 0, LastError) -> LastError;
do_validate(Domain, Validator, CN, Operation, EntityInfo, ModifierInfo, _Attempts, _) ->
    case ?GN_REG:whereis_name(Validator) of
        undefined ->
            {error,{internal_error,<<"Validator service is down">>}};
        Pid when is_pid(Pid) ->
            Ref = make_ref(),
            Msg = {'$call', {self(),Ref}, {validate, {Domain, CN, Operation, EntityInfo, ModifierInfo}}},
            MonRef = erlang:monitor(process,Pid),
            Pid ! Msg,
            receive
                {'DOWN',MonRef,process,Pid,_} ->
                    {error,{internal_error,<<"Validator service is down">>}};
                {'$reply',Ref,Result} ->
                    Result
            after 5000 ->
                {error,{internal_error,<<"Validator service timeout">>}}
            end
    end.


%% -----------------------------------------------------------
%% Callback to store changehistory
%% -----------------------------------------------------------
-spec store_changehistory_function(
              Domain :: binary() | atom(),
              CN :: binary(), ClassMeta::map(),
              Operation :: create | update | delete | clear | reload | corrupt,
              EntityInfo :: {Id::undefined | binary(), E0 :: undefined | map(), E1 :: undefined | map()} | undefined,
              ModifierInfo :: {Type::undefined | atom(),Id::undefined | binary()})
      -> ok | {error,Reason::term()}.
%% -----------------------------------------------------------
store_changehistory_function(_, _, _, _, _, _) -> ok;
store_changehistory_function(Domain, CN, _ClassMeta, 'clear'=Operation, _, ModifierInfo) ->
    NowTS = ?BU:timestamp(),
    E = build_ch(CN,Operation,ModifierInfo,NowTS),
    store_to_changehistory(Domain,E,NowTS);
store_changehistory_function(Domain, CN, _ClassMeta, Operation, {EntityId,Entity0,Entity1}, ModifierInfo) ->
    NowTS = ?BU:timestamp(),
    E = build_ch(CN,Operation,ModifierInfo,NowTS),
    {Data,ModFields} = case {Entity0,Entity1} of
                           {_,undefined} -> {Entity0,[]};
                           {undefined,_} -> {Entity1,[]};
                           {_,_} ->
                               Flds = maps:fold(fun(K,V,Acc) ->
                                                    case maps:get(K,Entity0,null) of
                                                        V -> Acc;
                                                        _ -> [K|Acc]
                                                    end end, [], maps:merge(Entity0,Entity1)),
                               {Entity1,lists:reverse(Flds)}
                       end,
    E1 = E#{<<"entity_id">> => EntityId,
            <<"modified_fields">> => ModFields,
            <<"data">> => Data},
    store_to_changehistory(Domain,E1,NowTS).

%% @private
build_ch(CN,Operation,ModifierInfo,NowTS) ->
    E = case ModifierInfo of
            undefined -> #{};
            {undefined,undefined} -> #{};
            {'fixturer',_} -> #{<<"modifier_type">> => <<"fixturer">>};
            {MType,undefined} -> #{<<"modifier_type">> => ?BU:to_binary(MType)};
            {undefined,MId} -> #{<<"modifier_id">> => ?BU:to_binary(MId)};
            {MType,MId} -> #{<<"modifier_type">> => ?BU:to_binary(MType),
                             <<"modifier_id">> => ?BU:to_binary(MId)}
        end,
    E#{<<"dt">> => ?BU:strdatetime3339(?BU:ticktoutc(NowTS)),
       <<"classname">> => CN,
       <<"operation">> => ?BU:to_binary(Operation)}.

%% @private
store_to_changehistory(Domain,MEItem,NowTS) ->
    Key = 'changehistory_state',
    Res = case get(Key) of
              {_,LastCheckTS,false} when NowTS-LastCheckTS<5000 -> skip;
              {_,LastCheckTS,true} when NowTS-LastCheckTS<5000 -> do;
              {_,LastCheckTS,{error,_}=Err} when NowTS-LastCheckTS<5000 -> Err;
              undefined -> {check,auto,false};
              {HC0,_,ToSave} -> {check,HC0,ToSave}
          end,
    case Res of
        skip -> ok;
        do -> do_store_to_changehistory(Domain,MEItem);
        {error,_}=Err1 -> Err1;
        {check,HC,DoSave} ->
            ReadOpts = #{<<"filter">> => [<<"==">>,[<<"property">>,<<"classname">>],?ModelEventsCN],
                         <<"mask">> => [<<"storage_mode">>]},
            case ?DMS_CACHE:read_cache(Domain,?ClassesCN,ReadOpts,HC) of
                {ok,not_changed} when not DoSave -> ok;
                {ok,not_changed} ->
                    do_store_to_changehistory(Domain,MEItem);
                {ok,[],HC1} ->
                    put(Key,{HC1,NowTS,false}),
                    ok;
                {ok,[Item],HC1} ->
                    case maps:get(<<"storage_mode">>,Item) of
                        <<"abstract">> ->
                            put(Key,{HC1,NowTS,false}),
                            ok;
                        _ ->
                            case ?DMS_CACHE:check(Domain,?ClassesCN) of
                                {error,_}=Err1 ->
                                    put(Key,{HC1,NowTS,Err1}),
                                    Err1;
                                {ok,_} ->
                                    put(Key,{HC1,NowTS,true}),
                                    do_store_to_changehistory(Domain,MEItem)
                            end end;
                {error,_}=Err1 ->
                    put(Key,{HC,NowTS,Err1}),
                    Err1
            end end.

%% @private
do_store_to_changehistory(Domain,MEItem) ->
    ClassItem = ?FixtureClasses:modelevents(Domain),
    Map0 = #{'object' => 'collection',
             'qs' => [],
             'initiator' => {system,undefined},
             'content' => MEItem},
    case ?DMS_CRUD:crud({Domain,'create',ClassItem,?ModelEventsCN,Map0,[]},fun(Reply) -> Reply end) of
        {'EXIT',Reason} ->
            ?LOG('$error',"Store to changehistory crashed: ~120tp",[Reason]),
            {error,{internal_error,?BU:strbin("ChangeHistory operation failure. See logs in '~ts'", [node()])}};
        {error,Reason} ->
            ?LOG('$error',"Store to changehistory error: ~120tp",[Reason]),
            {error,{internal_error,?BU:strbin("ChangeHistory operation error. See logs in '~ts'",[node()])}};
        {ok,_} -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================