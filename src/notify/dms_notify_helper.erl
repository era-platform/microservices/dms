%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.05.2021
%%% @doc

-module(dms_notify_helper).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_event_data/5,
         prepare_event_header/3,prepare_event_header/4]).
-export([get_subscriptions/5,
         get_subscriptions/4]).
-export([filtermap_subscriptions/3]).
-export([send/4,
         send_multi/1]).
-export([drop_subscriptions/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%%% ============================================================
%%% BUILD DATA
%%% ============================================================

%% ----------------------------
%% Build class event data map
%% ----------------------------
-spec build_event_data(Domain::binary(), ClassName::binary(),
                       Operation:: create | update | delete | clear | corrupt | reload,
                       EntityInfo:: undefined | {EId::binary(), E0::undefined | map(), E1::undefined | map()},
                       ModifierInfo:: {MType::undefined | binary(), MId::undefined | binary()}) -> map().
%% ----------------------------
build_event_data(_Domain, ClassName, Operation, EntityInfo, {MType,MId}) ->
    ED0 = #{<<"classname">> => ClassName, % class of model in clear internal format
            <<"classpath">> => <<>>, % path to class of model in rest (including <<"/rest/v1/...">>)
            <<"operation">> => operation_h(Operation)},
    ED1 = case Operation of
              O when O=='clear'; O=='reload' -> ED0;
              _ ->
                  {EntityId,Entity0,Entity1} = EntityInfo,
                  ED0#{<<"eid">> => EntityId,
                       <<"entity">> => case Operation of
                                           'delete' -> Entity0;
                                           'corrupt' when Entity1==undefined -> Entity0;
                                           _ -> Entity1
                                       end}
          end,
    ED2 = case MType of
              undefined -> ED1;
              _ -> ED1#{<<"modifier_type">> => MType}
          end,
    ED3 = case MId of
              undefined -> ED2;
              _ -> ED2#{<<"modifier_id">> => MId}
          end,
    ED3.

%% ----------------------------
%% Return event standard header
%% ----------------------------
prepare_event_header(EventClass,EventType,Event) ->
    prepare_event_header(EventClass,EventType,Event,?BU:timestamp()).
%%
prepare_event_header(EventClass,EventType,Event,EventTS)
    when EventClass == <<"modelevents">>, EventType == <<"data_changed">> ->
    CommonType = 'common',
    Pattern = {EventClass,CommonType,undefined},
    EventHeader = #{<<"event_class">> => EventClass,
                    <<"event_type">> => ?BU:to_binary(EventType),
                    <<"data">> => Event,
                    <<"event_ts">> => EventTS},
    From = self(),
    {Pattern,EventHeader,From}.

%%% ============================================================
%%% GET SUBSCRIPTIONS
%%% ============================================================

%% ----------------------------
%% Return subscriptions for model event, filtered by entity
%% ----------------------------
-spec get_subscriptions(Domain::binary(), EventName::binary(), ClassName::binary(), ClassMeta::map(), {EId::binary(),Entity0::map(),Entity1::map()}) ->
    {ok,[{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}]}.
%% ----------------------------
get_subscriptions(Domain, EventName, ClassName, ClassMeta, EntityInfo) ->
    case get_subscriptions(Domain,EventName,ClassName,ClassMeta) of
        {ok,[]} -> {ok,[]};
        {ok,[_|_]=S} -> filtermap_subscriptions(Domain,S,EntityInfo)
    end.

%% ----------------------------
%% Return subscriptions for model event, filtered by entity
%% ----------------------------
-spec get_subscriptions(Domain::binary(), EventName::binary(), ClassName::binary(), ClassMeta::map()) ->
    {ok,[{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}]}.
%% ----------------------------
get_subscriptions(Domain, EventName, ClassName, ClassMeta) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcSubscr,Domain),
    % if class metadata requires notification consistency, then retry for sometimes in some interval
    Fcheckretry = fun(Timeout,FunOut0) ->
                        Opts = maps:get('opts',ClassMeta),
                        case maps:get(<<"notify_integrity_timeout">>,Opts,0) of
                            0 ->
                                FunOut0(),
                                {ok,[]};
                            MaxTimeout ->
                                case process_info(self(),message_queue_len) of
                                    {_,QLen} when QLen > 100000 ->
                                        ?LOG('$warning',"Notify helper call to SUBSCR (search) retry: ~tp. Skip, queue too long",[QLen]),
                                        {ok,[]};
                                    _ ->
                                        ?LOG('$info',"Notify helper call to SUBSCR (search) retry...",[]),
                                        timer:sleep(erlang:min(MaxTimeout,Timeout)),
                                        retry_get_subscriptions(Domain, EventName, ClassName, ClassMeta, GlobalName, ?BU:timestamp(), MaxTimeout)
                                end end end,
    case catch ?GLOBAL:gen_server_call(GlobalName, {search,[[{EventName,ClassName}], [no_wildcards,local]]}) of
        {'EXIT',Reason} ->
            Fcheckretry(1000,fun() -> ?LOG('$crash',"Notify helper call to SUBSCR (search) crashed: ~120tp",[Reason]) end);
        {error,_}=Err ->
            Fcheckretry(1000,fun() -> ?LOG('$error',"Notify helper call to SUBSCR (search) error: ~120tp",[Err]) end);
        {retry_later,Timeout,Reason} ->
            % if class metadata requires notification consistency, then retry for sometimes in some interval
            Fcheckretry(Timeout,?LOG('$warning',"Notify helper call to SUBSCR (search) retry: ~120tp. Skip",[Reason]));
        {ok,[]} -> {ok,[]};
        {ok,[_|_]=S} -> {ok,S}
    end.

%% @private
retry_get_subscriptions(Domain, EventName, ClassName, _ClassMeta, GlobalName, StartTS, MaxTimeout) ->
    case catch ?GLOBAL:gen_server_call(GlobalName, {search,[[{EventName,ClassName}], [no_wildcards,local]]}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Notify helper call to SUBSCR (search) crashed: ~120tp",[Reason]),
            {ok,[]};
        {error,_}=Err ->
            ?LOG('$error',"Notify helper call to SUBSCR (search) error: ~120tp",[Err]),
            handle_error(Domain),
            {ok,[]};
        {retry_later,Timeout,Reason} ->
            case ?BU:timestamp() - StartTS of
                T when T >= MaxTimeout ->
                    ?LOG('warning',"Notify helper call to SUBSCR (search) retry timeout: ~120tp",[Reason]),
                    {ok,[]};
                T ->
                    timer:sleep(erlang:min(MaxTimeout-T,Timeout)),
                    retry_get_subscriptions(Domain, EventName, ClassName, _ClassMeta, GlobalName, StartTS, MaxTimeout)
            end;
        {ok,[]} -> {ok,[]};
        {ok,[_|_]=S} -> {ok,S}
    end.

%%% ============================================================
%%% FILTER
%%% ============================================================

%% ----------------------------
%% Return subscriptions for model event, filtered by entity
%% ----------------------------
-spec filtermap_subscriptions(Domain::binary(),
                              Subscriptions::[{SubId::binary(),SubValue::map()}],
                              EntityInfo::{Entity0::map() | undefined, Entity1::map() | undefined})
      -> {ok,[{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}]}.
%% ----------------------------
filtermap_subscriptions(Domain, Subscriptions, {_EId,Entity0,Entity1}) ->
    {ok,S1} = filter_subscriptions(Domain, Subscriptions, {Entity0,Entity1}),
    {ok,_S2} = merge_subscribers(S1).

%% ------------
%% @private
%% ------------
-spec filter_subscriptions(Domain::binary(), [{SubId::binary(),SubValue::map()}], {Entity0::map() | undefined, Entity1::map() | undefined}) ->
    {ok,[{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}]}.
%% ------------
filter_subscriptions(_, [], _) -> {ok,[]};
filter_subscriptions(Domain, Subscriptions, {Entity0,Entity1}) ->
    {ok, lists:filtermap(fun(Subscription) ->
                                case maps:get(<<"exargs">>,Subscription,undefined) of
                                    ExArgs when is_map(ExArgs); is_list(ExArgs) ->
                                        case ?BU:get_by_key(<<"filter">>,ExArgs,[]) of
                                            [] -> {true,{Subscription,true,true}};
                                            [_|_]=Filter ->
                                                try ?FilterFun:build_filter_fun(Filter) of
                                                    FilterFun when is_function(FilterFun,1) ->
                                                        Flt0 = FilterFun(Entity0),
                                                        Flt1 = case Entity1==Entity0 of
                                                                   true -> Flt0;
                                                                   false -> FilterFun(Entity1)
                                                               end,
                                                        case {Entity0,Entity1,Flt0,Flt1} of
                                                            {undefined,_,_,false} -> false; % create, filtered
                                                            {_,undefined,false,_} -> false; % delete, filtered
                                                            {_,_,false,false} -> false; % update, filtered
                                                            {_,_,Flt0,Flt1} -> {true,{Subscription,Flt0,Flt1}}
                                                        end
                                                catch _:_ ->
                                                    ?LOG('$warning',"'~ts' Notify 'modelevents.data_changed' in '~ts' found invalid filter: ~n\t~120tp",[Domain,Filter]),
                                                    false
                                                end;
                                            _ -> false % invalid filter
                                        end;
                                    _ -> {true,{Subscription,true,true}}
                                end end, Subscriptions)}.

%% @private
merge_subscribers([_]=Subscriptions) -> {ok,Subscriptions};
%% TODO ICP: temporary or not?
merge_subscribers(Subscriptions) -> {ok,Subscriptions}.
%%merge_subscribers(Subscriptions) ->
%%    Fget = fun(V) ->
%%                Subs = maps:get(subscriber,V),
%%                case maps:get(type,Subs,undefined) of
%%                    'token' -> {maps:get(id,Subs), maps:get(ts,Subs,0)};
%%                    _ -> {make_ref(),0}
%%                end end,
%%    Fextract = fun({{_,V},_,_}) when is_map(V) -> V;
%%                  ({_,V}) when is_map(V) -> V
%%               end,
%%    Subs = [erlang:append_element(Fget(Fextract(T)),T) || T <- Subscriptions],
%%    Subs1 = lists:sort(fun({Id1,_,_},{Id2,_,_}) when Id1 < Id2 -> true;
%%                          ({_Id,_V,_},{_Id,_V,_}) -> true;
%%                          ({_Id,V1,_},{_Id,V2,_}) when V1 > V2 -> true;
%%                          (_,_) -> false
%%                       end, Subs),
%%    Data = lists:ukeysort(1,Subs1),
%%    {ok,[SubscrT || {_,_,SubscrT} <- Data]}.

%%% ============================================================
%%% SEND
%%% ============================================================

%% ---------------------------------------------
%% Send 1 event to subscribers
%% ---------------------------------------------
-spec send(Subscriptions:: [{SubId::binary(),SubValue::map()}] | [{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}],
           EventH::map(),
           Operation:: create | update | delete | clear | reload | corrupt,
           ExOpts::list())
      -> ok.
%% ---------------------------------------------
send([],_,_,_) -> ok;
send(Subscriptions,EventH,Operation,ExOpts) ->
    send_multi([{Subscriptions,EventH,Operation,ExOpts}]).

%% ---------------------------------------------
%% Send portion of events. Subscribers list defined for every event
%% Builds and send by async mode sequently, but sync mode is portional
%% ---------------------------------------------
-spec send_multi([ { Subscriptions :: [{SubId::binary(),SubValue::map()}] | [{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}],
                     EventH :: map(),
                     Operation :: create | update | delete | clear | reload | corrupt,
                     ExOpts :: list() } ])
      -> ok.
%% ---------------------------------------------
send_multi([]) -> ok;
send_multi(ItemsToSend) ->
    SyncTaskItems = lists:foldl(fun({Subscriptions,EventH,Operation,ExOpts}, Acc) ->
                                    case send_single(Subscriptions,EventH,Operation,ExOpts) of
                                        [] -> Acc;
                                        [_|_]=List -> Acc ++ List % [{SubscrId::binary(),ExOpts::map(),SyncTask::map()}]
                                    end end, [], ItemsToSend),
    case SyncTaskItems of
        [] -> ok;
        _ ->
            SyncTasks = lists:map(fun({_,_,SyncTask}) -> SyncTask end, SyncTaskItems),
            Res = call_send_sync(SyncTasks, 5000),
            Zips = lists:zip(SyncTaskItems,Res),
            ToDel = lists:foldl(fun({_,ok},Acc) -> Acc;
                                   ({_,unavailable},Acc) -> Acc;
                                   ({{SubscrId,ExOpts,_}, {error,_}},Acc) -> [{SubscrId,ExOpts} | Acc]
                                end, [], Zips),
            case ToDel of
                [] -> ok;
                _ -> lists:foreach(fun({SubscrId,ExOpts}) -> del_subscription(SubscrId,ExOpts) end, lists:usort(ToDel))
            end end.

%% @private
%% -------------
-spec send_single(Subscriptions :: [{SubId::binary(),SubValue::map()}] | [{{SubId::binary(),SubValue::map()},Flt0::boolean(),Flt1::boolean()}],
                  EventH :: map(),
                  Operation :: create | update | delete | clear | reload | corrupt,
                  ExOpts :: list())
      -> [SyncTaskItem::{SubscrId::binary(),ExOpts::map(),SyncTask::map()}]. % ExOpts::#{domain}, SyncTask::#{destination,data}
%% -------------
send_single([],_,_,_) -> [];
send_single(Subscriptions, EventH, Operation, ExOpts) ->
    F = fun F(Subscription, Acc) when is_map(Subscription) ->
                    F({Subscription,true,true}, Acc);
            F({Subscription,Flt0,Flt1}, Acc) when is_map(Subscription) ->
                    SubscrId = maps:get(<<"sid">>,Subscription),
                    Subscr = maps:get(<<"subscriber">>,Subscription),
                    try case maps:get(<<"type">>,Subscr) of
                            'service' ->
                                [_Site,_Node,Pid] = ?BU:maps_get([<<"site">>,<<"node">>,<<"pid">>],Subscr),
                                case rpc:pinfo(Pid,status) of
                                    undefined ->
                                        del_subscription(SubscrId,ExOpts),
                                        Acc;
                                    {_,_} ->
                                        EventInfo = build_masked_event(EventH,{SubscrId,Subscription},{Operation,Flt0,Flt1}),
                                        ?LOG('$debug',"Notify ~p ! ~120tp", [Pid, {notify,SubscrId,EventInfo}]),
                                        Pid ! {notify,SubscrId,EventInfo},
                                        Acc
                                end;
                            _Type ->
                                % TODO: add other subscribers types. Ex websocket. If it is sync called, then return in list for multiple call by portion
                                del_subscription(SubscrId,ExOpts),
                                Acc
                        end
                    catch E:R:ST ->
                        ?LOG('$error',"Notify 'modelevents.data_changed' crashed: ~n\t~120tp~n\tStack: ~160tp",[{E,R},ST]),
                        del_subscription(SubscrId,ExOpts)
                    end end,
    lists:foldl(F,[],Subscriptions).

%% @private
build_masked_event(EventH,{SubscrId,Subscription},{Operation,Flt0,Flt1}) ->
    ExArgs = maps:get(<<"exargs">>,Subscription,undefined),
    Mask = case ExArgs of
               _ when is_map(ExArgs); is_list(ExArgs) ->
                   case ?BU:get_by_key(<<"mask">>,ExArgs,[]) of
                       [] -> [];
                       List when is_list(List) -> List;
                       B when is_binary(B) -> binary:split(B,[<<",">>,<<";">>,<<" ">>],[global,trim_all])
                   end;
               _ -> []
           end,
    ObjectMappings = case ExArgs of
                         _ when is_map(ExArgs); is_list(ExArgs) ->
                             ?BU:get_by_key(<<"rest_object_mapping">>,ExArgs,#{}); % <<"back_object_mapping">> also
                         _ -> undefined
                     end,
    Fshrink = fun(Data0) -> maps:without([<<"entity0">>],Data0) end,
    Fmask = fun(Data0) when Mask==[] -> Data0;
               (Data0) ->
                        case maps:get(<<"entity">>,Data0,undefined) of
                            undefined -> Data0;
                            E1 -> Data0#{<<"entity">> := maps:with(Mask,E1)}
                        end end,
    Fmapping = fun(Data0) when ObjectMappings==undefined -> Data0;
                  (Data0) ->
                        CN = maps:get(<<"classname">>,Data0),
                        case maps:get(CN,ObjectMappings,undefined) of
                            undefined -> Data0;
                            ClassPath -> Data0#{<<"classpath">> => ClassPath}
                        end end,
    Foperation = fun(Data0) ->
                        Operation1 = case Operation of
                                         'update' when not Flt0, Flt1 -> operation_e('create');
                                         'update' when Flt0, not Flt1 -> operation_e('delete');
                                         _ -> operation_e(Operation)
                                     end,
                        Data0#{<<"operation">> => Operation1}
                 end,
    EventH1 = EventH#{<<"data">> := Foperation(Fmapping(Fmask(Fshrink(maps:get(<<"data">>,EventH)))))},
    EventH1#{<<"sid">> => SubscrId}.

%% @private
%% -------------
-spec call_send_sync([SyncTask::map()], Timeout::non_neg_integer())
      -> [Result :: ok | unavailable | {error,Reason::term()}]. % Result has same sequence. SyncTask::#{destination,data}
%% -------------
call_send_sync([], _) -> [];
call_send_sync([SyncTask], Timeout) ->
    Dest = parse_destination(SyncTask),
    TaskData = parse_data(SyncTask),
    DefaultRef = make_ref(),
    case call(Dest,[{1,TaskData}],DefaultRef,Timeout) of
        DefaultRef -> [unavailable];
        [{1,Res}] -> Res
    end;
call_send_sync(SyncTasks, Timeout) ->
    Zips = lists:zip(lists:seq(1,length(SyncTasks)), SyncTasks),
    Map = lists:foldr(fun({SeqNum, SyncTask}, Acc) ->
                                Dest = parse_destination(SyncTask),
                                TaskData = parse_data(SyncTask),
                                case maps:get(Dest,Acc,undefined) of
                                    undefined -> Acc#{Dest => [{SeqNum,TaskData}]};
                                    TL -> Acc#{Dest => [{SeqNum,TaskData}|TL]}
                                end end, #{}, Zips),
    Dests = maps:to_list(Map),
    DefaultRef = make_ref(),
    case Dests of
        [{Dest,Tasks}] -> % call to one ws notify_multi
            % Tasks :: [{SeqNum, Data}]
            PureTasks = element(2,lists:unzip(Tasks)),
            case call(Dest,PureTasks,DefaultRef,Timeout) of
                DefaultRef -> lists:map(fun(_) -> 'unavailable' end, Tasks);
                Results -> Results
            end;
        [_|_] -> % multicall to several ws notify_multi
            F = fun(Dest,Tasks) ->
                     PureTasks = element(2,lists:unzip(Tasks)),
                     case call(Dest,PureTasks,DefaultRef,Timeout) of
                         DefaultRef -> lists:map(fun({SeqNum,_}) -> {SeqNum,'unavailable'} end, Tasks);
                         Results -> lists:map(fun({{SeqNum,_},Result}) -> {SeqNum,Result} end, lists:zip(Tasks,Results))
                     end end,
            KeyFuns = maps:fold(fun({Dest,Tasks},Acc) -> [{Dest,fun() -> F(Dest,Tasks) end} | Acc] end, [], Dests),
            Res = ?BLmulticall:apply(KeyFuns,Timeout+200),
            Union = lists:foldl(fun({_Dest,DestRes}, Acc1) ->
                                        lists:foldl(fun({SeqNum,TaskRes},Acc2) ->
                                                            [{SeqNum,TaskRes}|Acc2]
                                                    end, Acc1, DestRes)
                                end, [], Res),
            {_,Results} = lists:unzip(lists:keysort(1,Union)),
            Results
    end.

%% @private
parse_destination(SyncTask) when is_map(SyncTask) -> maps:get('destination',SyncTask).
%% @private
parse_data(SyncTask) when is_map(SyncTask) -> maps:get('data',SyncTask).

%% @private TODO: implement sync call to subscriber destinations by types
%% ----------
-spec call(Dest::term(),Tasks::[term()],Default::term(),Timeout::non_neg_integer())
      -> [TaskResult::term()] | term(). % same sequence of results
%% ----------
call(_Dest,_Tasks,Default,_Timeout) -> Default;
call(_Dest,Tasks,_Default,_Timeout) -> lists:map(fun({SeqNum,_}) -> {SeqNum,{error,{not_implemented,<<"Sync notify call not implemented">>}}} end, Tasks).

%% @private
del_subscription(SubscrId,ExOpts) ->
    ?LOG('$trace',"Subscription '~ts' automatically deleted.", [SubscrId]),
    DelReq = {del, [SubscrId]},
    Domain = ?BU:get_by_key('domain',ExOpts),
    GlobalName = ?GN_NAMING:get_globalname(?MsvcSubscr,Domain),
    ?GLOBAL:gen_server_cast(GlobalName, DelReq).

%%% ============================================================
%%% DROP SUBSCRIPTIONS
%%% ============================================================

%% ---------------------------------------------
-spec drop_subscriptions(Domain::binary(),EventName::binary(),CN::binary()) -> ok.
%% ---------------------------------------------
drop_subscriptions(Domain,EventName,ModelClass) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcSubscr,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName, {search,[[{EventName,ModelClass}], [no_wildcards]]}) of
        {'EXIT',Reason} ->
            ?LOG('$crash',"Notify helper call to SUBSCR (search) crashed: ~n\t~120tp",[Reason]),
            ok;
        {error,_}=Err ->
            ?LOG('$error',"Notify helper call to SUBSCR (search) error: ~n\t~120tp",[Err]),
            ok;
        {ok,[]} -> ok;
        {ok,[_|_]=S} ->
            F = fun({SubscrId,Subscription}) ->
                        Subscr = maps:get(<<"subscriber">>,Subscription),
                        case maps:get(<<"type">>,Subscr) of
                            'service' -> ok;
                            _Type ->
                                % TODO: add other subscribers types. Ex websocket
                                % TODO: close websocket connection or send message with expires=0
                                DelReq = {del, [SubscrId]},
                                ?GLOBAL:gen_server_cast(GlobalName, DelReq)
                        end end,
            lists:foreach(F, S)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
handle_error(_Domain) -> ok.

%% @private
operation_h(O) -> operation(O).

%% @private
operation_e(O) -> O.

%% @private
operation('delete') -> 0;
operation('create') -> 1;
operation('update') -> 2;
operation('clear') -> 3;
operation('corrupt') -> -1;
operation('reload') -> -2.