%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.07.2021
%%% @doc

-module(dms_notify).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([notify/7]).

-export([loop_subscr_pid/5,
         loop_sender_pid/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(NotifyHelper, dms_notify_helper).

-record(notify_state, {
    queue = [],
    subscriptions = [],
    subscriptions_ts = 0,
    %
    slowing_pause = 0 :: non_neg_integer(), % what performance reducing is now set on class.
    slowing_sends = 0 :: non_neg_integer(), % how many send operations should be done before slowing turned off.
    %
    state = '$idle' :: '$idle' | '$subscr' | 'sender',
    ref :: reference(),
    pid_subscr :: pid(),
    pid_sender :: pid()
}).

-define(SlowingQueueLen0, 500).
-define(SlowingQueueLen1, 1000).
-define(SlowingTimeout, 10).

%% ====================================================================
%% Public functions
%% ====================================================================

notify(Domain, CN, ClassMeta, Operation, EntityInfo, ModifierInfo, ExState) ->
    % build sequence id
%%    Seq = case erlang:get(notify_seq) of
%%              undefined -> 1;
%%              Seq0 -> Seq0+1
%%          end,
%%    erlang:put(notify_seq,Seq),
    Seq = 0,
    notify_stage_1(Seq,{Domain,CN,ClassMeta,Operation,EntityInfo,ModifierInfo},ExState).

%% @private next
notify_stage_1(Seq, {Domain, CN, ClassMeta, Operation, EntityInfo, ModifierInfo},ExState) ->
    NowTS = EventTS = ?BU:timestamp(),
    Data = {Domain, CN, Operation, EntityInfo, ModifierInfo},
    case ExState of
        #notify_state{queue=List,state=S} when S/='$idle' ->
            ?LOG('$debug',"NOTIFY ~ts, qlen: ~p. ~ts. State=~ts",[CN,length(List),Operation,S]),
            ExState1 = check_performance(ExState),
            ExState1#notify_state{queue=[{Seq,EventTS,Data}|List]};
        _ ->
            CachingTimeout = 0, % 10000 if it would be notified on changes in wssubscr
            case ExState of
                #notify_state{subscriptions_ts=TS,queue=List} when NowTS - TS < CachingTimeout ->
                    ?LOG('$debug',"NOTIFY ~ts, sync. ~ts",[CN,Operation]),
                    ExState1 = ExState#notify_state{queue=[{Seq,EventTS,Data}|List]},
                    notify_queued(ExState1);
                #notify_state{pid_subscr=PidSubscr,ref=Ref} when is_pid(PidSubscr) ->
                    ?LOG('$debug',"NOTIFY ~ts, async to pid. ~ts",[CN,Operation]),
                    PidSubscr ! {'apply',Ref,NowTS},
                    ExState#notify_state{queue=[{Seq,EventTS,Data}],state='$subscr'};
                _ ->
                    ?LOG('$debug',"NOTIFY ~ts, async start pid. ~ts",[CN,Operation]),
                    {Self,Ref} = {self(),make_ref()},
                    PidSubscr = spawn_link(fun() -> loop_subscr_pid(Domain,CN,ClassMeta,Self,Ref) end),
                    PidSubscr ! {'apply',Ref,NowTS},
                    #notify_state{queue=[{Seq,EventTS,Data}],
                                  state='$subscr',
                                  pid_subscr=PidSubscr,
                                  ref=Ref}
            end
    end.

%% @private
event_class() -> <<"modelevents">>.
%% @private
event_type() -> <<"data_changed">>.
%% @private
event_name() ->
    <<(event_class())/binary,".",(event_type())/binary>>.

%% @private next
notify_queued(#notify_state{subscriptions=[]}=ExState) -> check_performance(ExState#notify_state{state='$idle',queue=[]});
notify_queued(#notify_state{subscriptions=ClassSubscriptions,queue=Queue}=ExState) ->
    ?LOG('$debug',"NOTIFY -> ~p items",[length(Queue)]),
    {Mks1,ItemsToSend} = timer:tc(fun() -> lists:reverse(lists:filtermap(fun({Seq,EventTS,Data}) -> prepare_to_send(ClassSubscriptions,Seq,EventTS,Data) end, Queue)) end), % reverse reversed
    ?LOG('$debug',"   prep <- ~p mks",[Mks1]),
    send_notifies(ItemsToSend,ExState#notify_state{queue=[]}).

%% @private
prepare_to_send(ClassSubscriptions,_Seq,EventTS,{Domain, CN, Operation, EntityInfo, ModifierInfo}) ->
    try
    {ok,Subscriptions} = case Operation of
                             O when O=='clear'; O=='reload' -> {ok,ClassSubscriptions}; % No filter by entity
                             _ -> ?NotifyHelper:filtermap_subscriptions(Domain,ClassSubscriptions,EntityInfo) % Use filter by entity
                         end,
    case Subscriptions of
        [] -> false;
        _ ->
            Event = ?NotifyHelper:build_event_data(Domain,CN,Operation,EntityInfo,ModifierInfo),
            {_Pattern,EventH,_From}=_EventInfo = ?NotifyHelper:prepare_event_header(event_class(),event_type(),Event,EventTS),
            {true, {Subscriptions,EventH,Operation,[{domain,Domain}]}}
    end
    catch E:R:ST ->
        ?LOG('$error',"Notify prepare_to_send crashed: {~120tp, ~120tp}~n\tStack: ~120tp",[E,R,ST]),
        false
    end.

%% @private next
send_notifies([],ExState) -> check_performance(ExState#notify_state{state='$idle'});
send_notifies(ItemsToSend,#notify_state{pid_sender=PidSender,ref=Ref}=ExState) when is_pid(PidSender) ->
    PidSender ! {apply,Ref,ItemsToSend},
    ExState#notify_state{state='$sender'};
send_notifies(ItemsToSend,#notify_state{ref=Ref}=ExState) ->
    Self = self(),
    PidSender = spawn_link(fun() -> loop_sender_pid(Self,Ref) end),
    PidSender ! {'apply',Ref,ItemsToSend},
    ExState#notify_state{pid_sender=PidSender,state='$sender'}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_performance(#notify_state{queue=Q,slowing_pause=SP}=ExState) when length(Q) > ?SlowingQueueLen1, SP/=?SlowingTimeout ->
    %self() ! {reduce_performance,?SlowingTimeout},
    erlang:put('ex_slowing_pause',?SlowingTimeout),
    ExState#notify_state{slowing_pause=?SlowingTimeout,slowing_sends=3};
check_performance(#notify_state{slowing_sends=SS}=ExState) when SS>0 -> ExState;
check_performance(#notify_state{queue=Q,slowing_pause=SP}=ExState) when length(Q) < ?SlowingQueueLen0, SP/=0 ->
    %self() ! {restore_performance},
    erlang:put('ex_slowing_pause',0),
    ExState#notify_state{slowing_pause=0};
check_performance(ExState) -> ExState.

%% -------------------
loop_subscr_pid(Domain,CN,ClassMeta,NotifyPid,Ref) ->
    receive
        {'apply',Ref,NowTS} ->
            {Mks,{ok,ClassSubscriptions}} = timer:tc(fun() -> ?NotifyHelper:get_subscriptions(Domain,event_name(),CN,ClassMeta) end),
            ?LOG('$debug',"   get <- ~p mks",[Mks]),
            FunApply = fun(ExStateF) ->
                            % in notify pid
                            notify_queued(ExStateF#notify_state{subscriptions=ClassSubscriptions,subscriptions_ts=NowTS})
                       end,
            NotifyPid ! {'apply_exstate',FunApply},
            ?MODULE:loop_subscr_pid(Domain,CN,ClassMeta,NotifyPid,Ref);
        _ ->
            ?MODULE:loop_subscr_pid(Domain,CN,ClassMeta,NotifyPid,Ref)
    end.

%% -------------------
loop_sender_pid(NotifyPid,Ref) ->
    receive
        {'apply',Ref,ItemsToSend} ->
            Len = length(ItemsToSend),
            {Mks,_} = timer:tc(fun() -> ?NotifyHelper:send_multi(ItemsToSend) end),
            ?LOG('$debug',"   send <- ~p mks",[Mks]),
            FunApply = fun(#notify_state{queue=[]}=ExStateF) ->
                                % in notify pid
                                check_performance(ExStateF#notify_state{state='$idle',
                                                                        slowing_sends=0});
                          (#notify_state{pid_subscr=PidSubscr,slowing_sends=SS}=ExStateF) ->
                                % in notify pid
                                NowTS = ?BU:timestamp(),
                                PidSubscr ! {'apply',Ref,NowTS},
                                NewSS = case Len of A when A > ?SlowingTimeout -> SS; _ -> SS-1 end,
                                check_performance(ExStateF#notify_state{state='$subscr',
                                                                        slowing_sends=erlang:max(0,NewSS)})
                       end,
            NotifyPid ! {'apply_exstate',FunApply},
            ?MODULE:loop_sender_pid(NotifyPid,Ref);
        _ ->
            ?MODULE:loop_sender_pid(NotifyPid,Ref)
    end.