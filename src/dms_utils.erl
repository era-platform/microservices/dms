%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.05.2021
%%% @doc

-module(dms_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_value_for_site/1, get_value_for_site/2,
         make_atomic_keys/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------
%% Return value for current site from complex parameter
%% Value | #{Site => Value} | [#{<<"site">> => SiteReg, <<"value">> => Value}]
%% --------------------------------------------
-spec get_value_for_site(Term::[map()] | map() | term()) -> {ok,ValueForSite::term()} | false.
%% --------------------------------------------
get_value_for_site(Term) ->
    get_value_for_site(?PCFG:get_current_site(),Term).
get_value_for_site(CurSite,Map) when is_map(Map) ->
    Ref = make_ref(),
    case maps:get(CurSite,Map,Ref) of
        Ref -> false;
        Val -> {ok,Val}
    end;
get_value_for_site(CurSite,List) when is_list(List) ->
    F = fun(Item) -> {ok,maps:get(<<"value">>,Item,undefined)} end,
    lists:foldl(fun(Item,false) when is_map(Item) ->
        case maps:get(<<"site">>,Item,undefined) of
            Site when Site == CurSite -> F(Item);
            <<"/reg/",Re>> -> case re:run(CurSite,Re,[]) of nomatch -> false; {match,_} -> F(Item) end;
            _ -> false
        end;
        (_,Acc) -> Acc
                end, false, List);
get_value_for_site(_,Term) ->
    {ok,Term}.

%% --------------------------------------------
%% Change map or map list by transform first level keys to atoms
%% --------------------------------------------
make_atomic_keys(Item) when is_map(Item) ->
    maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_atom_new(K) => V} end, #{}, Item);
make_atomic_keys(Items) when is_list(Items) ->
    lists:map(fun(Item) ->  make_atomic_keys(Item) end, Items).

%% ====================================================================
%% Internal functions
%% ====================================================================