%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 08.05.2021
%%% @doc Data-Model Server microservice application.
%%%   Application could be setup by application:set_env:
%%%      group
%%%          Any value, unique to microservice type in configuration
%%%          Defines group of active-passive
%%%          Default: undefined (handles all classes)
%%%      order
%%%          Any integer value. The same order means static random
%%%          Defines order of leadership in active-passive group
%%%          Default: 0 (min priority)
%%%      master_dbstrings
%%%          List of postgresql connection strings (or aliases to corresponding configuraiton region)
%%%          Initial collections (storages and classes) are stored there.
%%%          Default: [] (could not work)
%%%      master_filestorage_type
%%%          Type of filestorage fixture on instance 'master'. <<"nfs">> | <<"fs">> | <<"fsync">>
%%%          Default: <<"nfs">>.

-module(dms).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0]).

-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -------------------------------------------
%% Start function
%% -------------------------------------------
-spec start() -> ok | {error,Reason::term()} | {retry_after,Timeout::non_neg_integer(),Reason::term()}.
%% -------------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    ?OUT('$info',"~ts. Application start (~120tp)", [?APP, self()]),
    setup_dependencies(),
    ensure_deps_started(),
    ?LeaderSupv:start_link(?LeaderCallbacks,?AppLeaderName).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts. Application stopped (~120tp)", [?APP, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% @private
setup_dependencies() ->
    % mnesia
    SchemaNodes = ?LeaderCallbacks:get_app_nodes(),
    ?BU:set_env(?MNESIALIB, 'log_destination', {dms,?MNESIALIB}),
    ?BU:set_env(?MNESIALIB, 'schema_nodes', SchemaNodes),
    ?BU:set_env(?MNESIALIB, 'schema_synchronized_function', fun() -> ok end),
    % dmlib
    ?BU:set_env(?DMLIB, 'log_destination', {dms,?DMLIB}),
    ?BU:set_env(?DMLIB, 'get_storages_function', fun ?DmlibCallbacks:get_storages/3),
    ?BU:set_env(?DMLIB, 'notify_function', fun ?DmlibCallbacks:notify_function/7),
    ?BU:set_env(?DMLIB, 'drop_subscriptions_function', fun ?DmlibCallbacks:drop_subscriptions/2),
    ?BU:set_env(?DMLIB, 'store_changehistory_function', fun ?DmlibCallbacks:store_changehistory_function/6),
    ?BU:set_env(?DMLIB, 'validate_function', fun ?DmlibCallbacks:validate_function/6),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that out of app link
%% --------------------------------------
ensure_deps_started() -> ok.