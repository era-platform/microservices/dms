%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc routines of data-model changes appliance
%%%     TODO ICP: apply class<->role_group binding

-module(dms_domain_model_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([refresh/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

refresh(NowTS,#dstate{model_classes=[]}=State) ->
    State1 = setup([],State),
    State2 = State1#dstate{meta_last_ts=NowTS,
                           model_hc=-1},
    {ok,State2,10}; % not 0 to avoid caching undefined globalname pid of dmlib domain
%%
refresh(NowTS,#dstate{domain=Domain,model_hc=HC}=State) ->
    case read(State) of
        {error,unregistered=Reason} ->
            % while DOMAIN gen_server of dmlib is unregistered (on start, for fast load)
            ?LOG('$error',"DMS. '~ts' read classes error: ~120tp",[Domain,Reason]),
            State2 = State#dstate{meta_last_ts=NowTS},
            {ok,State2,10};
        {error,Reason} when HC==-1 ->
            % WHILE CLASSES COLLECTION IS NOT LOADED
            ?LOG('$error',"DMS. '~ts' read classes error: ~120tp",[Domain,Reason]),
            State2 = State#dstate{meta_last_ts=NowTS},
            {ok,State2,100};
        {error,Reason} -> % model_hc >= 0, length(PrevItems) >= 2
            ?LOG('$error',"DMS. '~ts' read classes error: ~120tp",[Domain,Reason]),
            {ok,State#dstate{meta_last_ts=NowTS},10000};
        {ok,not_changed} ->
            ?FixtureHelper:setup_fixtures(Domain), % ensure that fixtures are in collection
            {ok,State#dstate{meta_last_ts=NowTS}};
        {ok,NewItems,HC1} ->
            State1 = setup(NewItems,State), % setup to dmlib
            ?FixtureHelper:setup_fixtures(Domain), % ensure that fixtures are in collection
            {ok,State1#dstate{meta_last_ts=NowTS,
                              model_hc=HC1}}
    end.

%% @private
read(#dstate{domain=Domain,model_hc=HC}) when HC==-1 ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcType,Domain),
    case ?GN_REG:whereis_name_nocache(GlobalName) of
        undefined -> {error,unregistered};
        Pid when is_pid(Pid) ->
            SyncOpts = #{'cache_globalname' => false,
                         'timeout_sync' => 5000,
                         'attempts' => 20,
                         'timeout_between_attempts' => 10},
            ?DMS_CACHE_SYNC:read_cache_sync(Domain,?ClassesCN,#{},HC,SyncOpts)
    end;
read(#dstate{domain=Domain,model_hc=HC}) ->
    ?DMS_CACHE:read_cache(Domain,?ClassesCN,#{},HC).

%% @private
setup(NewItems,#dstate{domain=Domain}=State) ->
    % combine with fixtures
    Fixtures = [?FixtureClasses:storages(Domain), ?FixtureClasses:classes(Domain)],
    FixtureIds = lists:map(fun(Item) -> maps:get(<<"id">>,Item) end, Fixtures),
    FixtureCNs = lists:map(fun(Item) -> maps:get(<<"classname">>,Item) end, Fixtures),
    NewItems1 = lists:filter(fun(Item) ->
                                not lists:member(maps:get(<<"id">>,Item),FixtureIds)
                                    and not lists:member(maps:get(<<"classname">>,Item),FixtureCNs)
                             end, NewItems),
    NewItems2 = Fixtures ++ NewItems1,
    % prepare for dmlib
    NewItems3 = decorate_items(Domain,filter_items(nest_items(Domain,map_items(NewItems2)))),
    % setup to dmlib
    ?DMLIB:setup_classes(Domain,NewItems3),
    State#dstate{model_classes=NewItems3}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------
%% Aprior mapping. Make atomic keys
%% ------------------------------
map_items(Items) ->
    ?U:make_atomic_keys(Items).

%% ------------------------------
%% Build nested props
%% ------------------------------
nest_items(Domain,Items) ->
    EmptyId = ?BU:emptyid(),
    AdapterMap = #{emptyid => EmptyId,
                   fun_name => fun(Item) -> maps:get(classname,Item) end,
                   fun_id => fun(Item) -> maps:get(id,Item) end,
                   fun_parent => fun(Item) -> maps:get(parent_id,Item,EmptyId) end,
                   fun_properties => fun(Item) -> maps:get(properties,Item) end,
                   fun_set_properties => fun(Item,Props) -> Item#{properties => Props} end,
                   fun_propname => fun(Prop) -> maps:get(<<"name">>,Prop) end,
                   fun_logwarn => fun(S) -> ?LOG('$warning', "~ts. '~ts' ~ts",[?APP,Domain,S]) end},
    ?BU:nest_items(Items,AdapterMap).

%% ------------------------------
%% Filter items (by storage_mode, by groups).
%% TODO ICP: filter by groups
%% ------------------------------
filter_items(Items) ->
    % by storage_mode (no abstracts)
    Items1 = lists:filter(fun(Item) -> maps:get('storage_mode',Item) /= <<"abstract">> end, Items),
    % by groups (only if class linked to group, or not linked and current group is default (minimal index on site))
    case ?CFG:group() of
        undefined -> Items1;
        Group ->
            F = fun() ->
                    MsvcGroups = lists:map(fun({_Node,MsvcParams}) -> maps:get(group,MsvcParams,undefined) end,
                                           ?PCFG:get_msvc_opts(?MsvcType)),
                    _IsDefault = Group == lists:min(MsvcGroups) and not lists:member(undefined,MsvcGroups)
                end,
            IsDefault = ?PCFG:cache({?MODULE,?FUNCTION_NAME,is_default_group},[],F),
            CurSite = ?PCFG:get_current_site(),
            lists:filter(fun(Item) ->
                                Setup = maps:get(<<"dms_group">>,maps:get(opts,Item),#{}),
                                case maps:get(CurSite, Setup, undefined) of
                                    undefined -> IsDefault;
                                    Group -> true;
                                    ClassGroup when is_integer(ClassGroup); is_binary(ClassGroup) -> false;
                                    ClassGroups when is_list(ClassGroups) -> lists:member(Group,ClassGroups)
                                end end, Items1)
    end.

%% ------------------------------
%% Build ready for use items. Structure optimized for work, etc.
%% ------------------------------
decorate_items(Domain,Items) -> [decorate_item(Domain,Item) || Item <- Items].
decorate_item(_Domain,Item) ->
    maps:without([ext],Item).
