%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc General data model server.
%%%        Has global name, handles all outside requests.
%%%        Routes request to dmlib class servers.
%%%        Subscribes on changes of DC's classes collection.
%%%      Opts:
%%%        regname, domain, cache_ets

-module(dms_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/2]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(CacheFilterTimeout, 60000 + ?BU:random(5000)).
-define(MetaRefreshTimeout, 60000 + ?BU:random(5000)). % TODO ICP: use default subscription on dc class 'classes'

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Name, Opts) when is_map(Opts) ->
    ?GLOBAL:gen_server_start_link({global, Name}, ?MODULE, Opts#{'name' => Name}, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) when is_map(Opts) ->
    [RegName, Domain, CacheEts] = ?BU:extract_required_props(['regname','domain','cache_ets'], Opts),
    % ----
    Ref = make_ref(),
    gen_server:cast(self(),{'init',Ref}),
    % ----
    State = #dstate{regname = RegName,
                    domain = Domain,
                    site = ?PCFG:get_current_site(),
                    self = self(),
                    ref = Ref,
                    initts = ?BU:timestamp(),
                    subscrid = SubscrId=?BU:strbin("sid_DMS:~p;~ts",[?CFG:group(),Domain]),
                    cache_ets = CacheEts,
                    cache_ref = Ref,
                    cache_timerref = erlang:send_after(?CacheFilterTimeout, self(), {'timer_cache',Ref}),
                    meta_ref = Ref,
                    meta_timerref = erlang:send_after(0, self(), {'timer_refresh_meta',Ref})},
    % ----
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?ClassesCN,self(),SubscrId),
    % ----
    ?LOG('$info', "~ts. '~ts' facade inited", [?APP, Domain]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% --------------
%% returns current node
handle_call({get_node}, _From, State) ->
    {reply, {node(), self()}, State};

%% --------------
%% print state
handle_call({print}, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$force', " ~ts. '~ts' facade state: ~n\t~120tp", [?APP, Domain, State]),
    {reply, ok, State};

%% --------------
%% restart gen_serv
handle_call({restart}, _From, State) ->
    {stop, restart, ok, State};

%% --------------
%% Crud call
handle_call({crud,_ClassName,_Args}=Req, From, #dstate{dmlib_pid=SrvPid}=State) ->
    ?DMLIB:crud(SrvPid, fun(Reply) -> gen_server:reply(From,Reply) end, Req),
    {noreply,State};

%% Crud call with sync in timeout
handle_call({crud,ClassName,Args,SyncTimeout}, From, #dstate{dmlib_pid=SrvPid}=State) ->
    Req1 = {crud,ClassName,Args},
    ?DMLIB:crud(SrvPid, fun(Reply) -> gen_server:reply(From,Reply) end, Req1, SyncTimeout),
    {noreply,State};

%% --------------
%% get_hc
handle_call({get_hc,ClassName}, From, #dstate{dmlib_pid=SrvPid}=State) ->
    ?DMLIB:get_hc(SrvPid, ClassName, fun(Reply) -> gen_server:reply(From,Reply) end),
    {noreply,State};

%% get_hc with sync in timeout
handle_call({get_hc,ClassName,SyncTimeout}, From, #dstate{dmlib_pid=SrvPid}=State) ->
    ?DMLIB:get_hc(SrvPid, ClassName, fun(Reply) -> gen_server:reply(From,Reply) end, SyncTimeout),
    {noreply,State};

%% --------------
%% other
handle_call(_Msg, _From, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown call: ~120tp", [?APP,Domain,_Msg]),
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({'init',Ref}, #dstate{ref=Ref,domain=Domain}=State) ->
    SrvRegName = ?DMLIB:get_regname(Domain),
    ChildSpec = ?DMLIB:get_domain_childspec(Domain,SrvRegName),
    StartRes = ?DSUPV:start_child(Domain,ChildSpec),
    ?LOG('$trace', "'~ts'. Start dmlib child: ~120tp", [Domain, StartRes]),
    % or (if it should be linked to dmlib's supv, but then deletion should be implemented)
    % Res = ?DMLIB:add_domain(Domain),
    % {ok,_SupvPid,SrvRegName} = Res,
    State1 = State#dstate{dmlib_regname=SrvRegName,
                          dmlib_pid=whereis(SrvRegName)},
    {noreply, State1};

%% --------------
%% crud by cast
handle_cast({'$call', {FromPid,Ref}, {crud,_ClassName,_Args}=Req}, #dstate{dmlib_pid=SrvPid}=State) ->
    ?DMLIB:crud(SrvPid, fun(Reply) -> FromPid ! {'$reply',Ref,Reply}, ok end, Req),
    {noreply,State};

%% --------------
%% other
handle_cast(_Msg, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown cast: ~120tp", [?APP,Domain,_Msg]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% crud by info
handle_info({'$call', {FromPid,Ref}, {crud,_ClassName,_Args}=Req}, #dstate{dmlib_pid=SrvPid}=State) ->
    ?DMLIB:crud(SrvPid, fun(Reply) -> FromPid ! {'$reply',Ref,Reply}, ok end, Req),
    {noreply,State};

%% --------------
%% Timer of cache filtering
handle_info({'timer_cache',Ref}, #dstate{cache_ref=Ref,cache_ets=_ETS}=State) ->
    Ref1 = make_ref(),
    State2 = State#dstate{cache_ref=Ref1,
                          cache_timerref = erlang:send_after(?CacheFilterTimeout, self(), {'timer_cache',Ref1})},
    {noreply, State2};

%% --------------
%% Timer to refresh classes
handle_info({'timer_refresh_meta',Ref}, #dstate{meta_ref=Ref,domain=Domain}=State) ->
    NowTS = ?BU:timestamp(),
    Self = self(),
    Ref1 = make_ref(),
    Pid = spawn(fun() ->
                    {Timeout2,State2} = try ?DModelCfg:refresh(NowTS,State) of
                                            {ok,State1} -> {?MetaRefreshTimeout,State1};
                                            {ok,State1,Timeout1} -> {Timeout1,State1}
                                        catch C:R:ST ->
                                            ?LOG('$crash'," ~ts. '~ts' facade state: ~n\tStack: ~160tp", [?APP, Domain, {C,R,ST}]),
                                            {1000,State}
                                        end,
                    Self ! {'refresh_meta_result',Ref1,{Timeout2,State2}}
                end),
    MonRef = erlang:monitor(process,Pid),
    {noreply,State#dstate{meta_ref = Ref1,
                          meta_refreshing_pid = Pid,
                          meta_refreshing_monref = MonRef,
                          meta_refreshing_queued = false,
                          meta_timerref = undefined}};

%% On async refresh process down
handle_info({'DOWN',MonRef,process,Pid,_Reason},#dstate{meta_refreshing_pid=Pid,meta_refreshing_monref=MonRef}=State) ->
    Ref1 = make_ref(),
    Timeout = 10000,
    NewState1 = State#dstate{meta_ref=Ref1,
                             meta_timerref = erlang:send_after(Timeout, self(), {'timer_refresh_meta',Ref1}),
                             meta_refreshing_pid = undefined,
                             meta_refreshing_monref = undefined,
                             meta_refreshing_queued = false},
    {noreply, NewState1};
%%
handle_info({'DOWN',_MonRef,process,_Pid,_Reason},#dstate{meta_refreshing_pid=undefined}=State) ->
    {noreply, State};

%% Async result of refresh classes
handle_info({'refresh_meta_result',Ref,{Timeout,NewState}}, #dstate{meta_ref=Ref,meta_refreshing_queued=IsQueued}) ->
    Timeout1 = case IsQueued of
                   true -> 1000;
                   false -> Timeout
               end,
    Ref1 = make_ref(),
    NewState1 = NewState#dstate{meta_ref=Ref1,
                                meta_timerref = erlang:send_after(Timeout1, self(), {'timer_refresh_meta',Ref1}),
                                meta_refreshing_pid = undefined,
                                meta_refreshing_monref = undefined,
                                meta_refreshing_queued = false},
    {noreply, NewState1};

%% --------------
%% Notify about changes in classes on standby
handle_info({notify,SubscrId,_EventInfo}, #dstate{subscrid=SubscrId,meta_timerref=TimerRef,meta_last_ts=LastTS,meta_refreshing_pid=undefined}=State) ->
    ?BU:cancel_timer(TimerRef),
    Timeout = case ?BU:timestamp() - LastTS of
                  T when T<1000 -> 1000-T;
                  _ -> 100
              end,
    Ref1 = make_ref(),
    State1 = State#dstate{meta_ref=Ref1,
                          meta_timerref = erlang:send_after(Timeout, self(), {'timer_refresh_meta',Ref1})},
    {noreply, State1};

%% Notify about changes in classes on active refreshing
handle_info({notify,SubscrId,_EventInfo}, #dstate{subscrid=SubscrId,meta_timerref=undefined}=State) ->
    State1 = State#dstate{meta_refreshing_queued = true},
    {noreply, State1};

%% --------------
handle_info(_Info, #dstate{domain=Domain}=State) ->
    ?LOG('$trace', "~ts. '~ts' Unknown info: ~120tp", [?APP,Domain,_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #dstate{}=_State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
