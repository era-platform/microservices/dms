%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc Configuration functions to get app opts

-module(dms_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([group/0,
         order/0,
         master_dbstrings/0,
         master_filestorage_type/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% return group of active-passive
group() ->
    ?BU:get_env(?APP,'group',undefined).

%% return leadership order in group
order() ->
    ?BU:get_env(?APP,'log_destination',0).

%% return master dbstrings to initial load and store basic collections (storages, classes)
master_dbstrings() ->
    DbStrings = ?BU:get_env(?APP,'master_dbstrings',[]),
    lists:filtermap(fun(<<"alias://",_/binary>>=Alias) ->
                            case ?PCFG:get_alias_value(Alias) of
                                {ok,Value} -> {true,Value};
                                {error,_} -> false
                            end;
                       (Value) -> {true,Value}
                    end, DbStrings).

%% return master filestorage type for fixture of file storages
master_filestorage_type(Default) ->
    case ?BU:get_env(?APP,'master_filestorage_type',Default) of
        <<"fs">>=T -> T;
        <<"nfs">>=T -> T;
        <<"fsync">>=T -> T;
        _ -> Default
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
