%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.05.2021
%%% @doc Callback module for leader-election.

-module(dms_leader_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_app_nodes/0,
         get_top_supv_mfa/1,
         log/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Callback functions
%% ====================================================================

%% --------------
get_app_nodes() ->
    FunLog = fun(Fmt,Args) -> ?LOG('$info',?BU:str("LEADER '~ts'. ~ts", [?AppLeaderName,Fmt]),Args) end,
    ?LeaderU:get_app_nodes(?APP, ?MsvcType, FunLog).

%% --------------
get_top_supv_mfa(_) ->
    {?SUPV,start_link,[]}.

%% --------------
log(Fmt,Args) ->
    ?LOG('$info',Fmt,Args).

%% ====================================================================
%% Internal functions
%% ====================================================================