%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.05.2021
%%% @doc Helper for fixture storing (closure from fixtures into data)

-module(dms_fixture_helper).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup_fixtures/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Ensure fixtures of controlled classes (classes, storages), return worth result
%% --------------------------------------
-spec setup_fixtures(Domain::binary() | atom()) -> {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
setup_fixtures(Domain) ->
    ?DMS_FIXTURE_HELPER:worth_result([setup_classes(Domain),setup_storages(Domain),setup_modelevents(Domain)]).

%% @private
setup_classes(Domain) ->
    FixtureItems = [?FixtureClasses:storages(Domain),?FixtureClasses:classes(Domain)],
    {Ids,CNs} = lists:unzip(lists:map(fun(Item) -> {maps:get(<<"id">>,Item),maps:get(<<"classname">>,Item)} end, FixtureItems)),
    FunFilter = fun(Item) -> lists:member(maps:get(<<"id">>,Item),Ids) orelse lists:member(maps:get(<<"classname">>,Item),CNs) end,
    case ?DMS_CACHE:read_cache(Domain,?ClassesCN,#{},auto) of
        {ok,LoadedItems,_} ->
            LoadedItems1 = lists:filter(FunFilter, LoadedItems),
            ?DMS_FIXTURE_HELPER:setup_classes(Domain,FixtureItems,LoadedItems1);
        _ -> {error,read_failed}
    end.

%% @private
setup_modelevents(Domain) ->
    FixtureItems = [?FixtureClasses:modelevents(Domain)],
    {Ids,CNs} = lists:unzip(lists:map(fun(Item) -> {maps:get(<<"id">>,Item),maps:get(<<"classname">>,Item)} end, FixtureItems)),
    CmpFields = [<<"id">>,<<"classname">>,<<"properties">>],
    FunFilterMap = fun(Item) ->
                        case lists:member(maps:get(<<"id">>,Item),Ids) orelse lists:member(maps:get(<<"classname">>,Item),CNs) of
                            false -> false;
                            true -> {true,maps:with(CmpFields,Item)}
                        end end,
    FunShrinkFixtureCmp = fun(Item) -> maps:with(CmpFields,Item) end,
    ?DMS_FIXTURE_HELPER:setup_fixtures(Domain,?ClassesCN,FixtureItems,FunFilterMap,FunShrinkFixtureCmp).

%% @private
setup_storages(Domain) ->
    FixtureItems = ?FixtureStorages:master_storages(Domain),
    FunFilter = fun(Item) -> maps:get(<<"instance">>,Item) == ?MasterStorageInstance end,
    case ?DMS_CACHE:read_cache(Domain,?StoragesCN,#{},auto) of
        {ok,LoadedItems,_} ->
            LoadedItems1 = lists:filter(FunFilter, LoadedItems),
            ?DMS_FIXTURE_HELPER:ensure_fixtures(Domain,?StoragesCN,FixtureItems,LoadedItems1);
        _ -> {error,read_failed}
    end.
