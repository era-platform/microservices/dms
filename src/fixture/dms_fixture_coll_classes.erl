%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.05.2021
%%% @doc DMS is initial model microservice. It works over itself.
%%%      But nowhere to store it's own collections: storages and classes
%%%      Initial collections are declared here.

-module(dms_fixture_coll_classes).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([storages/1,
         classes/1,
         modelevents/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% CLASS "storages"
%% return fixtured entity of controlled class
%% -------------------------------------
storages(Domain) ->
    decorate_by_validator(Domain,storages()).

%% @private
storages() ->
    #{
        <<"id">> => ?BU:to_guid(<<"aad09bca-0179-a956-cd14-7cd30a921f58">>),
        <<"classname">> => ?StoragesCN,
        <<"name">> => <<"Storages">>,
        <<"description">> => <<"General platform collection. Storage instances.">>,
        <<"storage_mode">> => ?MasterStorageMode,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator",[?MsvcType]),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"max_limit">> => 10000,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<>>,
            <<"lookup_properties">> => [<<"type">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"type">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"postgresql">>,<<"kafka">>,<<"clickhouse">>,<<"s3">>,<<"nfs">>,<<"fs">>,<<"fsync">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"instance">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"default">> => <<>>,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"priority">>,
                <<"data_type">> => <<"integer">>,
                <<"required">> => false,
                <<"default">> => 0,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"params">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"opts">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% -------------------------------------
%% CLASS "classes"
%% return fixtured entity of controlled class
%% -------------------------------------
classes(Domain) ->
    decorate_by_validator(Domain,classes()).

%% @private
classes() ->
    #{
        <<"id">> => ?BU:to_guid(<<"04de953d-0179-a956-d5bc-7cd30a921f58">>),
        <<"classname">> => ?ClassesCN,
        <<"name">> => <<"Classes">>,
        <<"description">> => <<"General platform collection. Classes.">>,
        <<"storage_mode">> => ?MasterStorageMode,
        <<"cache_mode">> => <<"full">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"validator">> => ?BU:strbin("~ts:validator",[?MsvcType]),
            <<"notify">> => true,
            <<"check_required_fill_defaults">> => true,
            <<"replace_without_read">> => false,
            <<"max_limit">> => 10000,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<>>,
            <<"lookup_properties">> => [<<"classname">>],
            <<"store_changehistory_mode">> => <<"sync">>
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"classname">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"name">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"default">> => <<"">>,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"description">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => false,
                <<"default">> => <<"">>,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"parent_id">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => false,
                <<"default">> => ?BU:emptyid(),
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"properties">>,
                <<"data_type">> => <<"any">>,
                <<"merge_levels">> => 1,
                <<"required">> => true,
                <<"multi">> => true
            },
            #{
                <<"name">> => <<"storage_mode">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"ram">>, <<"runtime">>, <<"category">>, <<"history">>, <<"transactionlog">>, <<"abstract">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"cache_mode">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"none">>, <<"temp">>, <<"full">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"integrity_mode">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"async">>, <<"sync_fast_read">>, <<"sync_fast_notify">>, <<"sync">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"opts">>,
                <<"data_type">> => <<"any">>,
                <<"merge_levels">> => 1,
                <<"required">> => true,
                <<"multi">> => false
            }
        ]}.

%% -------------------------------------
%% CLASS "modelevents"
%% return fixtured entity of controlled class
%% -------------------------------------
modelevents(Domain) ->
    decorate_by_validator(Domain,modelevents()).

%% @private
modelevents() ->
    #{
        <<"id">> => ?BU:to_guid(<<"049dc874-017a-7b6d-ca5f-7cd30a921f58">>),
        <<"classname">> => ?ModelEventsCN,
        <<"name">> => <<"ModelEvents">>,
        <<"description">> => <<"General platform collection. Model Events, history of changes.">>,
        <<"storage_mode">> => <<"history">>,
        <<"cache_mode">> => <<"none">>,
        <<"integrity_mode">> => <<"sync_fast_read">>,
        <<"opts">> => #{
            <<"notify">> => false,
            <<"check_required_fill_defaults">> => false,
            <<"replace_without_read">> => true,
            <<"max_size">> => 10000,
            <<"storage_instance">> => ?MasterStorageInstance,
            <<"partition_property">> => <<"dt">>,
            <<"partition_interval">> => <<"month">>,
            <<"partition_count">> => 2,
            <<"replication_factor">> => 2,
            <<"notify_transactions">> => false,
            <<"store_changehistory_mode">> => <<"none">>,
            <<"lookup_properties">> => [<<"classname">>,<<"entity_id">>,<<"modifier_id">>]
        },
        <<"properties">> => [
            #{
                <<"name">> => <<"dt">>,
                <<"data_type">> => <<"datetime">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"classname">>,
                <<"data_type">> => <<"string">>,
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"operation">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"create">>,<<"replace">>,<<"update">>,<<"delete">>,<<"clear">>,<<"undo_clear">>],
                <<"required">> => true,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"entity_id">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => false,
                <<"default">> => ?BU:emptyid(),
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"modifier_type">>,
                <<"data_type">> => <<"enum">>,
                <<"items">> => [<<"user">>,<<"fixturer">>,<<"system">>],
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"modifier_id">>,
                <<"data_type">> => <<"uuid">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"modified_fields">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            },
            #{
                <<"name">> => <<"data">>,
                <<"data_type">> => <<"any">>,
                <<"required">> => false,
                <<"multi">> => false
            }
        ]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
decorate_by_validator(Domain,Items) when is_list(Items) ->
    lists:map(fun(Item) -> decorate_by_validator(Domain,Item) end, Items);
decorate_by_validator(Domain,Item) when is_map(Item) ->
    case ?ValidatorClasses:decorate_fixture(Domain,Item) of
        {ok,Item1} -> Item1;
        {error,_} -> Item
    end.
