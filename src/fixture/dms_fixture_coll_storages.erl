%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.05.2021
%%% @doc

-module(dms_fixture_coll_storages).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([master_storages/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% STORAGES OF INSTANCE "master"
%% return list of fixtured entities of storage instance 'master'
%%    where general persistent collections: storages, classes are located.
%% -------------------------------------
%% For 'root' domain - from configuruation
%% -------------------------------------
master_storages('root'=Domain) ->
    case build_master_data_storages(Domain, ?CFG:master_dbstrings()) of
        [_|_]=ItemsData ->
            ItemsFile = build_master_file_storages(Domain),
            decorate_by_validator(Domain,ItemsData++ItemsFile);
        [] ->
            ?LOG('$error',"Invalid 'master_dbstrings' in configuration opts of microservice 'dms'. Expected list of connection strings or existing aliases (keys: host,port,login,pwd,database)",[]),
            []
    end;
%% -------------------------------------
%% For non root domains - from outer settings of domain in parent domain's entity
%% -------------------------------------
master_storages(Domain) when is_binary(Domain) ->
    ParentDomain = case ?BU:extract_parent_domain(Domain) of
                       <<>> -> 'root';
                       B ->
                           case ?GN_REG:whereis_name(?GN_NAMING:get_globalname(?MsvcType,B)) of
                               Pid when is_pid(Pid) -> B;
                               undefined -> 'root'
                           end end,
    master_storages(ParentDomain,Domain).

%% @private
master_storages(ParentDomain,Domain) ->
    ReadOpts = #{<<"filter">> => [<<"==">>,[<<"property">>,<<"name">>],Domain]},
    case ?DMS_CACHE:read_cache(ParentDomain,?DomainsCN,ReadOpts,auto) of
        {error,Reason} ->
            ?LOG('$error',"~ts. Cannot detect master storages for '~ts'. Domain '~ts' access error: ~n\t~120tp",[?APP,Domain,ParentDomain,Reason]);
        {ok,[],_} ->
            ?LOG('$error',"~ts. Cannot detect master storages for '~ts'. Domain not found in '~ts'",[?APP,Domain,ParentDomain]),
            [];
        {ok,[DomainItem],_} ->
            case build_master_data_storages(Domain, maps:get(<<"master_dbstrings">>,DomainItem,[])) of
                [_|_]=ItemsData ->
                    ItemsFile = build_master_file_storages(Domain),
                    decorate_by_validator(Domain,ItemsData++ItemsFile);
                [] ->
                    ?LOG('$error',"Invalid 'master_dbstrings' in configuration opts of microservice 'dms'. Expected list of connection strings (keys: host,port,login,pwd,database)",[]),
                    []
            end end.

%% @private
build_master_data_storages(Domain,DbStrings) ->
    lists:filtermap(fun({DbString,N}) ->
                            String = ?BU:to_list(DbString),
                            ParsedParams = [extract(K,String,undefined) || K <- [host,port,login,pwd,database]],
                            case lists:all(fun(undefined) -> false; (_) -> true end, ParsedParams) of
                                false -> false;
                                true -> {true, build_master_storage(?MasterStorageType, Domain, N, ParsedParams)}
                            end end, lists:zip(DbStrings,lists:seq(1,length(DbStrings)))).

%% @private
build_master_file_storages(_Domain) ->
    case ?CFG:master_filestorage_type(<<"nfs">>) of
        <<"nfs">> ->
            [#{
                <<"id">> => ?BU:to_guid(?BU:md5(?BU:to_binary(erlang:phash2(<<"nfs">>)))),
                <<"type">> => <<"nfs">>,
                <<"instance">> => ?MasterStorageInstance,
                <<"priority">> => 10,
                <<"params">> => #{<<"configkey">> => <<"attachments">>},
                <<"opts">> => #{}
            }];
        <<"fs">> ->
            [#{
                <<"id">> => ?BU:to_guid(?BU:md5(?BU:to_binary(erlang:phash2(<<"fs">>)))),
                <<"type">> => <<"fs">>,
                <<"instance">> => ?MasterStorageInstance,
                <<"priority">> => 10,
                <<"params">> => #{<<"configkey">> => <<"attachments">>},
                <<"opts">> => #{}
            }];
        <<"fsync">> ->
            [#{
                <<"id">> => ?BU:to_guid(?BU:md5(?BU:to_binary(erlang:phash2(<<"fsync">>)))),
                <<"type">> => <<"fsync">>,
                <<"instance">> => ?MasterStorageInstance,
                <<"priority">> => 10,
                <<"params">> => #{<<"prefix">> => <<"master">>},
                <<"opts">> => #{}
            }]
    end.

%% @private
extract(Key,String,Default) ->
    case re:run(String,?BU:str("~ts:([^,]*)",[Key])) of
        nomatch -> Default;
        {match,[{_,_},{From,Len}]} -> ?BU:to_binary(string:substr(String,From+1,Len))
    end.

%% @private
build_master_storage(?MasterStorageType, _Domain, SeqNum, [Host,Port,Login,Pwd,Database]=Params) ->
    #{
        <<"id">> => ?BU:to_guid(?BU:md5(?BU:to_binary(erlang:phash2(Params)))),
        <<"type">> => ?MasterStorageType,
        <<"instance">> => ?MasterStorageInstance,
        <<"priority">> => SeqNum,
        <<"params">> => #{
            <<"host">> => Host,
            <<"port">> => Port,
            <<"login">> => Login,
            <<"pwd">> => Pwd,
            <<"database">> => Database
        },
        <<"opts">> => #{}
    }.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
decorate_by_validator(Domain,Items) when is_list(Items) ->
    lists:map(fun(Item) -> decorate_by_validator(Domain,Item) end, Items);
decorate_by_validator(Domain,Item) when is_map(Item) ->
    case ?ValidatorStorages:decorate_fixture(Domain,Item) of
        {ok,Item1} -> Item1;
        {error,_} -> Item
    end.

