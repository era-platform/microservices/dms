%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.05.2021

%% ====================================================================
%% Types
%% ====================================================================

%% state for domain gen_server
-record(dstate, {
    regname :: binary(), % gen_server reg name
    domain :: binary(),
    site :: binary(),
    self :: pid(),
    ref :: reference(),

    initts :: non_neg_integer(),

    dmlib_regname :: atom(),
    dmlib_pid :: pid(),

    subscrid :: binary(), % Id for subscription to modelevents

    model_classes = [] :: [map()], % actual data of data-model classes from dc.
    model_hc = -1 :: term(), % hashcode of dc classes cached result

    cache_ets :: reference(), % ets for domain cache purposes (economy ets count for all classes).
    cache_ref :: reference(),
    cache_timerref :: reference(),

    meta_ref :: reference(),
    meta_timerref :: reference() | undefined,
    meta_last_ts = 0 :: integer(),
    meta_refreshing_pid :: pid() | undefined,
    meta_refreshing_monref :: reference() | undefined,
    meta_refreshing_queued = false :: boolean
}).

-define(MasterStorageInstance, <<"master">>).
-define(MasterStorageType, <<"postgresql">>).
-define(MasterStorageMode, <<"category">>).

-define(EventsStorageMode, <<"history">>).
-define(EventsStorageInstance, <<"events">>).

-define(ClassesCN, <<"classes">>).
-define(StoragesCN, <<"storages">>).
-define(ModelEventsCN, <<"modelevents">>).
-define(DomainsCN, <<"domains">>).

-define(MsvcSubscr, <<"subscr">>).

%% ====================================================================
%% Constants and defaults
%% ====================================================================

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(PLATFORMLIB, platformlib).
-define(MNESIALIB, mnesialib).
-define(DMLIB, dmlib).

-define(APP, dms).
-define(MsvcType, <<"dms">>).
-define(AppLeaderName, dms_app_leader).

-define(SUPV, dms_supv).
-define(SRV, dms_srv).
-define(CFG, dms_config).
-define(U, dms_utils).
-define(LeaderCallbacks, dms_leader_callback).
-define(DmlibCallbacks, dms_dmlib_callback).
-define(DomainTreeCallback, dms_domaintree_callback).

-define(DSUPV, dms_domain_supv).
-define(DSRV, dms_domain_srv).
-define(DModelCfg, dms_domain_model_config).

-define(FixtureClasses, dms_fixture_coll_classes).
-define(FixtureStorages, dms_fixture_coll_storages).
-define(FixtureHelper, dms_fixture_helper).

-define(VALIDATOR, dms_validator_srv).
-define(ValidatorClasses, dms_validator_coll_classes).
-define(ValidatorStorages, dms_validator_coll_storages).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLlog, basiclib_log).
-define(BLmulticall, basiclib_multicall).
-define(FilterFun, basiclib_filter_fun).

%% ------
%% From platformlib
%% ------
-define(PCFG, platformlib_config).

-define(GLOBAL, platformlib_globalnames_global).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GN_REG, platformlib_globalnames_registrar).

-define(LeaderSupv, platformlib_leader_app_supv).
-define(LeaderU, platformlib_leader_app_utils).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CACHE_SYNC, platformlib_dms_cache_sync).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_CRUD, platformlib_dms_crud).

-define(DTREE_SUPV, platformlib_domaintree_supv).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {dms,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?LOGFILE, {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?LOGFILE, Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?LOGFILE, {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?LOGFILE, Text)).

%% ====================================================================
%% Define other
%% ====================================================================
